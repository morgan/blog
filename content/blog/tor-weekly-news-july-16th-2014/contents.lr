title: Tor Weekly News — July 16th, 2014
---
pub_date: 2014-07-16
---
author: lunar
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the twenty-eighth issue of Tor Weekly News in 2014, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what is happening in the Tor community.</p>

<h1>Roundup of research on incentives for running Tor relays</h1>

<p>As an hors-d’œuvre to the now <a href="https://petsymposium.org/2014/" rel="nofollow">on-going the Privacy Enhancing Technology Symposium</a>, Rob Jansen wrote <a href="https://blog.torproject.org/blog/tor-incentives-research-roundup-goldstar-par-braids-lira-tears-and-torcoin" rel="nofollow">a long blog post covering the last five years of research on incentives for running Tor relays</a>.</p>

<p>Rob introduces the topic by describing the current “volunteer resource model” and mentions that “has succeeded so far: Tor now consists of over 5000 relays transferring between 4 and 5 GiB/s in aggregate”. Rob lists several possible reasons why volunteers run relays right now. They are all intrinsic motivations: current operators run relays because they really want to.</p>

<p>Is only relying on volunteers going to limit the growth of the Tor network in the future? There are already <a href="https://www.torservers.net/" rel="nofollow">not-for-profit organizations</a> operating relays based on donations, but growing them too much would also be problematic. Another area being explored are extrinsic motivations: making Tor clients faster when someone runs a relay or giving a financial reward — in a currency or another — for the service. Some can legitimately ask if they are <a href="http://p2pfoundation.net/Intrinsic_vs._Extrinsic_Motivation#Why_Extrinsic_Motivation_Doesn.27t_Work" rel="nofollow">suitable for Tor at all</a> and Rob raises plenty of legitimate concerns on how they would interact with the current set of volunteers.</p>

<p>The problem keeps interesting researchers, and Rob details no less than six schemes: the oldest are <a href="http://cs.gmu.edu/~astavrou/research/Par_PET_2008.pdf" rel="nofollow">PAR</a> and <a href="http://freehaven.net/anonbib/papers/incentives-fc10.pdf" rel="nofollow">Gold Star</a> which introduced anonymity problems, <a href="http://www.robgjansen.com/publications/braids-ccs2010.pdf" rel="nofollow">BRAIDS</a> where double spending of rewards is prevented without leaking timing information, <a href="http://www.robgjansen.com/publications/lira-ndss2013.pdf" rel="nofollow">LIRA</a> which focused on scalability, <a href="http://www.robgjansen.com/publications/tears-hotpets2014.pdf" rel="nofollow">TEARS</a> where a publicly auditable e-cash protocol reduce the reliance on trusted parties, and finally, the (<a href="https://www.torproject.org/docs/trademark-faq#researchpapers" rel="nofollow">not ideally named</a>) <a href="http://www.robgjansen.com/publications/torpath-hotpets2014.pdf" rel="nofollow">TorCoin</a> which introduces the idea of a crypto-currency based on “proof-of-bandwidth”.</p>

<p>Rob details the novel ideas and drawbacks of each schemes, so be sure to read the original blog post for more details. After this roundup, Rob highlights that “recent research has made great improvements in the area of Tor incentives”. But that’s for the technical side as “it is unclear how to make headway on the social issues”.</p>

<p>“Tor has some choices to make in terms of how to grow the network and how to position the community during that growth process” concludes Rob. So let’s have that conversation.</p>

<h1>Defending against guard discovery attacks with layered rotation time</h1>

<p>Guard nodes are a key component of a Tor client’s anonymity. Once an attacker gains knowledge of which guard node is being used by a particular client, putting the guard node under monitoring is likely the last step before finding a client’s IP address.</p>

<p>George Kadianakis has restarted the <a href="https://lists.torproject.org/pipermail/tor-dev/2014-July/007122.html" rel="nofollow">discussion</a> on how to <a href="https://bugs.torproject.org/9001" rel="nofollow">slow down guard discovery of hidden services</a> by exploring the idea of “keeping our middle nodes more static”. The idea is to slow down the attacks based on repeated circuit destruction by reusing the same “middle nodes for 3-4 days instead of choosing new ones for every circuit”. Introducing this new behavior will slow down the attack, but George asks “are there any serious negative implications?”</p>

<p>The idea is not new, as Paul Syverson <a href="https://lists.torproject.org/pipermail/tor-dev/2014-July/007125.html" rel="nofollow">pointed out</a>: “Lasse and I suggested and explored the idea of layered guards when we introduced guards”. He adds “there are lots of possibilities here”.</p>

<p>George worries that middle nodes would then “always see your traffic coming through your guard (assuming a single guard per client)”. Ian Goldberg <a href="https://lists.torproject.org/pipermail/tor-dev/2014-July/007123.html" rel="nofollow">added</a> “the exit will now know that circuits coming from the same middle are more likely to be the same client”. Restricting the change to only hidden services and not every client means that it will be “easy for an entry guard to learn whether a client has static middle nodes or not”.</p>

<p>As George puts it the <a href="https://lists.torproject.org/pipermail/tor-dev/2014-July/007126.html" rel="nofollow">latest message in the thread</a>: “As always, more research is needed…” Please help!</p>

<h1>More monthly status reports for June 2014</h1>

<p>The wave of regular monthly reports from Tor project members for the month of June continued, with submissions from <a href="https://lists.torproject.org/pipermail/tor-reports/2014-July/000587.html" rel="nofollow">Michael Schloh von Bennewitz</a> and <a href="https://lists.torproject.org/pipermail/tor-reports/2014-July/000588.html" rel="nofollow">Andrew Lewman</a>.</p>

<p>Arturo Filastò reported on behalf of <a href="https://lists.torproject.org/pipermail/tor-reports/2014-July/000586.html" rel="nofollow">the OONI team</a>, while Roger Dingledine submitted the <a href="https://lists.torproject.org/pipermail/tor-reports/2014-July/000589.html" rel="nofollow">SponsorF report</a></p>

<h1>Miscellaneous news</h1>

<p>The various roadmaps that came out of the <a href="https://trac.torproject.org/projects/tor/wiki/org/meetings/2014SummerDevMeeting" rel="nofollow">2014 summer dev. meeting</a> have been <a href="https://trac.torproject.org/projects/tor/wiki/org/meetings/2014SummerDevMeeting/Roadmaps" rel="nofollow">transcribed</a> in a joint effort by George Kadianakis, Yawning Angel, Karsten Loesing, and an anonymous person. Most items will probably be matched with a ticket soon.</p>

<p>The Tor Project is hiring a <a href="https://www.torproject.org/about/jobs-controller.html" rel="nofollow">financial controller</a>. This is a part time position, approximately 20 hours per week, at the office in Cambridge, Massachusetts.</p>

<p>The Tails developers announced the creation of two new mailing lists. “<a href="https://mailman.boum.org/pipermail/tails-dev/2014-July/006330.html" rel="nofollow">If you are a designer, UX/UI expert or beginner</a>” interested in the theory and practice of designing user interfaces for Tails, the <a href="https://mailman.boum.org/listinfo/tails-ux" rel="nofollow">tails-ux list</a> is for you, while the <a href="https://mailman.boum.org/listinfo/tails-project" rel="nofollow">tails-project list</a> is dedicated to “<a href="https://mailman.boum.org/pipermail/tails-dev/2014-July/006329.html" rel="nofollow">the ‘life’ of the project</a>“; however, “technical questions should stay on tails-dev”.</p>

<p>Alan kicked of the aforementioned tails-ux mailing list <a href="https://mailman.boum.org/pipermail/tails-ux/2014-July/000000.html" rel="nofollow">announcing progress</a> on Tails initial login screen. The new set of mockups is visible on the corresponding <a href="https://tails.boum.org/blueprint/tails-greeter:_revamp_UI/" rel="nofollow">blueprint</a>.</p>

<p>More mockups! Nima Fatemi <a href="https://lists.torproject.org/pipermail/tor-dev/2014-July/007115.html" rel="nofollow">produced</a> some for a possible browser-based Tor control panel, incorporating features that were lost with the removal of Vidalia from the Tor Browser, such as the world map with Tor circuit visualizations. “How would you perfect <a href="https://people.torproject.org/~nima/ux/about-tor.png" rel="nofollow">that image</a>? What’s missing?”, asked Nima, hoping “to inspire people to start hacking on it”.</p>

<p>Meanwhile, Sean Robinson <a href="https://lists.torproject.org/pipermail/tor-dev/2014-July/007136.html" rel="nofollow">had been working</a> on a new graphical Tor controller called <a href="https://gitorious.org/syboa/syboa" rel="nofollow">Syboa</a>. Sean’s “primary motivation for Syboa was to replace TorK, so <a href="https://gitorious.org/syboa/syboa/source/7082a82:docs/screenshot-basic.png" rel="nofollow">it looks</a> more like TorK than Vidalia”. Sean announces that he will not have time for further development soon but that he would answer questions.</p>

<p>Juha Nurmi <a href="https://lists.torproject.org/pipermail/tor-reports/2014-July/000590.html" rel="nofollow">submitted</a> the weekly status report for the ahmia.fi GSoC project.</p>

<p>Thanks to the <a href="https://lists.torproject.org/pipermail/tor-mirrors/2014-July/000623.html" rel="nofollow">University of Edinburgh’s School of Informatics</a>, <a href="https://lists.torproject.org/pipermail/tor-mirrors/2014-July/000624.html" rel="nofollow">funcube.fr</a>, <a href="https://lists.torproject.org/pipermail/tor-mirrors/2014-July/000627.html" rel="nofollow">Stefano Fenoglio</a>, <a href="https://lists.torproject.org/pipermail/tor-mirrors/2014-July/000632.html" rel="nofollow">IP-Connect</a>, <a href="https://lists.torproject.org/pipermail/tor-mirrors/2014-July/000633.html" rel="nofollow">Justin Ramos</a>, <a href="https://lists.torproject.org/pipermail/tor-mirrors/2014-July/000634.html" rel="nofollow">Jacob Henner from Anatomical Networks</a>, and <a href="https://lists.torproject.org/pipermail/tor-mirrors/2014-July/000638.html" rel="nofollow">Hackabit.nl</a> for running mirrors of the Tor Project website!</p>

<h1>Tor help desk roundup</h1>

<p>Users often ask about for assistance setting up Tor Cloud instances. Sina Rabbani is taking over the maintenance of Tor Cloud and is working on updating the packages and documentation. Until new documentation on using the up-to-date images and Amazon Web Services interface lands, users not already familiar with AWS may want to use a different virtual server provider to host their bridges.</p>

<h1>Easy development tasks to get involved with</h1>

<p>The setup scripts of the Flashproxy and Obfsproxy pluggable transports attempt to download and build the M2Crypto library if they are not already installed. We´d really want to avoid this and have the setup script fail if not all libraries are present for building Flashproxy. The ticket that describes this bug also outlines <a href="https://bugs.torproject.org/10847#comment:4" rel="nofollow">a possible workaround that disables all downloads during the setup process</a>. If you know a bit about setuptools and want to turn this description into a patch and test it, please give it a try.</p>

<p>This issue of Tor Weekly News has been assembled by Lunar, harmony, Matt Pagan, Karsten Loesing, and George Kadianakis.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

