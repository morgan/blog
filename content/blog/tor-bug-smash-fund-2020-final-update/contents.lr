title: Bug Smash Fund, Year 2: Progress Since February 2021
---
pub_date: 2021-07-13
---
author: alsmith
---
tags:

fundraising
bug smash fund
---
categories: fundraising
---
summary:

Last August, we asked you to help us fundraise during our second annual Bug Smash Fund campaign. We want to share an update on some of the work that the second year of the Bug Smash Fund has made possible. Since 2019, we've marked 410 tickets with BugSmashFund. As of today, 373 of those tickets have been closed, and 37 of them are still in progress. 
---
_html_body:

<p>Last August, we asked you to help us fundraise during our second annual<a href="https://blog.torproject.org/tor-bug-smash-fund-2020-106K-raised"> Bug Smash Fund campaign</a>. This fund is designed to grow a healthy reserve earmarked for maintenance work, finding bugs, and smashing them—all tasks necessary to keep Tor Browser, the Tor network, and the many tools that rely on Tor strong, safe, and running smoothly.<strong> In 2020, despite the challenges of COVID-19 and event cancellations, you helped us to raise $106,709!</strong></p>
<p>We want to share an update on some of the work that the second year of the Bug Smash Fund has made possible.</p>
<p>Since 2019, we’ve marked 410 tickets with BugSmashFund. As of today, 373 of those tickets have been closed, and 37 of them are still in progress. This year, we've used the Bug Smash Fund to work on continuous integration tooling, Tor Browser improvements, helping onion services providers defend against DDoS by migrating to v3 onion services, fixing bugs on GetTor, moving forward with<a href="https://youtu.be/Xpg775CJkaY"> </a><a href="https://blog.torproject.org/announcing-arti">Arti</a>, and security fixes. We have also used the Bug Smash Fund to create a new<a href="https://status.torproject.org"> status.torproject.org</a> page, which will act as a landing place for network and service status updates.</p>
<p>Thanks for supporting this work!</p>
<p>Below is a list of some of the tickets we’ve closed <a href="https://blog.torproject.org/tor-bug-smash-fund-yr2-progress">since our last update in February 2021</a>.</p>
<h3>Website</h3>
<p>We fixed several bugs on our main website (<a href="https://torproject.org">https://torproject.org</a>). </p>
<ul>
<li aria-level="1"><a href="https://gitlab.torproject.org/tpo/web/tpo/-/issues/64">On the new download page, the signature and the (?) link are not perceived as different</a></li>
<li aria-level="1"><a href="https://gitlab.torproject.org/tpo/web/tpo/-/issues/146">Should we move anonbib to the Tor website?</a></li>
<li aria-level="1"><a href="https://gitlab.torproject.org/tpo/web/tpo/-/issues/203">Update to v3 onion service links</a></li>
</ul>
<h3>Censorship Analysis</h3>
<p>We tracked several censorship events in Iran, Venezuela and other countries.</p>
<ul>
<li aria-level="1"><a href="https://gitlab.torproject.org/tpo/anti-censorship/censorship-analysis/-/issues/26807">Venezuela blocks access to the Tor network</a></li>
</ul>
<h3>BridgeDB</h3>
<p>We have a sponsored project to improve bridgeDB, but some bugs are not covered and were fixed with the Bug Smash Fund.</p>
<ul>
<li aria-level="1"><a href="https://gitlab.torproject.org/tpo/anti-censorship/bridgedb/-/issues/27984">bridgedb verifyHostname doesn't check subjectAltName extension</a></li>
</ul>
<h3>GetTor</h3>
<p>We are back into maintaining GetTor. It will be integrated into <a href="https://gitlab.torproject.org/tpo/anti-censorship/rdsys">rdsys</a> this year, but for now, we have GetTor running on its own.</p>
<ul>
<li aria-level="1"><a href="https://gitlab.torproject.org/tpo/anti-censorship/gettor-project/gettor/-/issues/80">gettor not answering emails April 27</a></li>
</ul>
<h3>Onionoo</h3>
<p>We fixed a few bugs in the service that runs <a href="https://metrics.torproject.org/rs.html#search">Relays Search</a>.</p>
<ul>
<li aria-level="1"><a href="https://gitlab.torproject.org/tpo/network-health/metrics/onionoo/-/issues/40005">"AS" prefix missing from the as field in documents</a></li>
<li aria-level="1"><a href="https://gitlab.torproject.org/tpo/network-health/metrics/onionoo/-/issues/27187">Possible for inconsistency between summary and details with AS number</a></li>
<li aria-level="1"><a href="https://gitlab.torproject.org/tpo/network-health/metrics/onionoo/-/issues/40003">Add an ant task to update GeoIP resources</a></li>
</ul>
<h3>Tor</h3>
<p>We fixed a variety of bugs on core tor with the Bug Smash Fund over the last several months.</p>
<ul>
<li aria-level="1"><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/5825">Bridges without geoip file report empty statistics</a></li>
<li aria-level="1"><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/1802">ControlPort GETCONF does not recognize command aliases</a></li>
<li aria-level="1"><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/2362">"GETINFO config-text" adds spurious DataDirectory, Log entries</a></li>
<li aria-level="1"><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/7956">Tor uses Roaming (remote) %APPDATA% instead of %LOCALAPPDATA%</a></li>
<li aria-level="1"><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/9860">junk log messages every time SETCONF changes the set of ORPorts</a></li>
<li aria-level="1"><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/10027">Tor Windows service should be installed with the NetworkService account</a></li>
<li aria-level="1"><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/15607">Tor log dates imprecise</a></li>
<li aria-level="1"><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/17953">Fallback to resolving localhost when interface searches fail</a></li>
<li aria-level="1"><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/18263">GETCONF provides incorrect value when undefined</a></li>
<li aria-level="1"><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/19138">Received extra server info (size 0)</a></li>
<li aria-level="1"><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/17901">Tor would bind ControlPort to public ip address if it has no localhost interface</a></li>
<li aria-level="1"><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/24876">Directory Authorities should test reachability of relays in their family</a></li>
<li aria-level="1"><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/32320">Jenkins Windows builders are currently broken</a></li>
<li aria-level="1"><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/31832">Coverage flapping in hs_get_responsible_hsdirs()</a></li>
<li aria-level="1"><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/30409">Some of our tests require internet connectivity / an IPv4 stack</a></li>
<li aria-level="1"><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/30816">Remove ping ::1 from tor's test-network-all and simplify the logic</a></li>
<li aria-level="1"><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/28899">nondeterministic coverage of dirvote.c and shared_random.c</a></li>
<li aria-level="1"><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/27722">rust protover doesn't canonicalize adjacent and overlapping ranges</a></li>
<li aria-level="1"><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/27739">rust protover_all_supported() accepts too-long protocol names</a></li>
<li aria-level="1"><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/27207">Examples in CodingStandardsRust.md are wrong</a></li>
<li aria-level="1"><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/27190">disparate duplicate subproto handling in protover</a></li>
<li aria-level="1"><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/27191">handling double spaces in protover</a></li>
<li aria-level="1"><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/27198">protover doesn't forbid version zero</a></li>
<li aria-level="1"><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/26436">Check uses of CMP_SEMANTIC for IP addresses</a></li>
<li aria-level="1"><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/23523">Handle extreme values better in add_laplace_noise()</a></li>
<li aria-level="1"><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/23323">sample_laplace_distribution should produce a valid result on 0.0</a></li>
<li aria-level="1"><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/23414">rep_hist_format_hs_stats() should add noise, then round</a></li>
<li aria-level="1"><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/23415">sample_laplace_distribution() should take multiple random inputs</a></li>
<li aria-level="1"><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/22760">Fix extra-info flags on fallbacks</a></li>
<li aria-level="1"><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/18562">Do we need to chown AF_UNIX sockets?</a></li>
<li aria-level="1"><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/18308">Use a better pattern for "create mutex if not already initialized"</a></li>
<li aria-level="1"><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/17673">circuit_handle_first_hop assumes all one-hop circuits are directory circuits</a></li>
<li aria-level="1"><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/16849">clear_status_flags_on_sybil might want to clear more flags</a></li>
<li aria-level="1"><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/13297">compute_weighted_bandwidths() broken for dirauths</a></li>
<li aria-level="1"><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/9925">Directory Authorities can crash client/relay by scrambling microdesc assignments</a></li>
<li aria-level="1"><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/10481">connection_mark_unattached_ap_:  checking always true edge_has_sent_end</a></li>
<li aria-level="1"><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/40175">zlib compression bomb warning in notices.log on a middle relay</a></li>
<li aria-level="1"><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/40224">Find a working alternative to using MaxMind's GeoLite2 databases</a></li>
</ul>
<h3>Tor Browser</h3>
<p>We’ve fixed bugs in the Tor Browser building process, as well as closed two tickets related to Tor Browser itself: <a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40478">onion alias url rewrite is broken</a> and <a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/18311">document first party isolation for Tor researchers</a>. We are also in the process of changing the repositories branch from “master” to “main,” and some of that work was done during this period.</p>
<p>Thank you to everybody who made a contribution to the Bug Smash Fund. This work is critical in helping us to provide safer tools for millions of people around the world exercising their human rights to privacy and freedom online.</p>
<p>If you’d like to make a contribution to the Bug Smash Fund, you can do so by making a gift at<a href="https://donate.torproject.org"> donate.torproject.org</a>: just add “Bug Smash Fund” into the comment field, and we’ll make sure it’s directed to the right place.</p>

