title: Dreaming at Dusk: the Tor Project’s NFT Auction & What’s Next
---
pub_date: 2021-05-28
---
author: alsmith
---
tags: fundraising
---
categories: fundraising
---
summary:

In mid-May, the Tor Project held a nonfungible token (NFT) auction of a generative art piece we called Dreaming at Dusk, created by artist Itzel Yard (ixshells) and derived from the private key of the first onion service, Dusk.

This action was held on Foundation and resulted in a final bid of 500 Ethereum (ETH), roughly $2M USD at the time of the auction, with the proceeds going towards the Tor Project and our work to improve and promote Tor.
---
_html_body:

<p>In mid-May, the Tor Project held a <a href="https://en.wikipedia.org/wiki/Non-fungible_token">nonfungible token</a> (NFT) <a href="https://foundation.app/torproject/dreaming-at-dusk-35855">auction of a generative art piece we called Dreaming at Dusk</a>, created by artist Itzel Yard (<a href="https://foundation.app/ixshells">ixshells</a>) and derived from the private key of the first onion service, Dusk.</p>
<p>This action was held on <a href="https://foundation.app">Foundation</a> and resulted in a final bid of 500 Ethereum (ETH), roughly $2M USD at the time of the auction, with the proceeds going towards the Tor Project and our work to improve and promote Tor.</p>
<p>Raising roughly $2M USD in one day breaks all records of individual giving we could possibly imagine, and we are extremely humbled and grateful for the success of this auction and what this means for the Tor Project nonprofit organization.</p>
<p>We deeply appreciate everyone who shared this effort and followed along, and want to share more about why we held this auction, the artist <a href="https://foundation.app/ixshells">ixshells</a>, and what happens next with the money raised.</p>
<h2>Why auction an NFT?</h2>
<p>If you have been following the Tor Project, you will know that 2020 was a difficult year for the organization (as for many nonprofits, small businesses, and people). <a href="https://blog.torproject.org/covid19-impact-tor">We made the difficult decision to lay off one third of our staff in April 2020</a>. Beyond the challenges brought on by COVID-19 and economic changes in 2020, Open Technology Fund—a long-time supporter and funder of the Tor Project and other efforts in our ecosystem—<a href="https://blog.torproject.org/save-open-technology-fund">faced a political attack that froze its funds</a> and halted one of our contracts.</p>
<p>Despite these challenges, we have made strong strides in regaining solid financial footing, much of which is a result of the <a href="https://blog.torproject.org/use-a-mask-use-tor-thank-you">2020 year-end campaign</a> (#UseAMaskUseTor) and your generous support during this time. We’ve also been able to re-hire several staff members and about a year on from that moment, we are in a better place.</p>
<p>Still, these disruptions made it very clear that our goal of diversifying our <a href="https://blog.torproject.org/category/tags/financial-statements">funding sources</a> is critical, and that having a solid reserve of general operating funds would help us weather any future storms and keep the Tor Project a strong nonprofit for a long time. We are always looking for strategies to raise these kinds of funds.</p>
<p>Over the last several months, Tor community members have discussed the idea of auctioning an NFT as a fundraising campaign—could an effort like this help to raise general operating funds and keep Tor strong? After <a href="https://foundation.app/Snowden/stay-free-edward-snowden-2021-24437">Freedom of the Press Foundation had such an epic success with their NFT auction with Edward Snowden</a> for the piece called Stay Free, and after we saw that our specific audience responded positively to this auction, we decided to hold an auction of our own.</p>
<h2>The NFT &amp; the artist</h2>
<p>We wanted to honor a piece of Tor history with this process, and with the deprecation of v2 onion services coming up rapidly, we decided to honor <a href="https://lists.torproject.org/pipermail/tor-talk/2006-August/001770.html">the very first .onion website known as Dusk</a>, or duskgytldkxiuqc6.onion. We decided that the winner of our auction would receive two things: (1) the private cryptographic key used to create Dusk, and (2) a one-of-a-kind art piece generated using this key. We wanted this to be an opportunity to own a piece of history from the origins of the decentralized internet.</p>
<p></p><center><a href="https://foundation.app/torproject/dreaming-at-dusk-35855"><img height="640" src="https://lh3.googleusercontent.com/pw/ACtC-3f3mJiGGjSWpNeMDluzj6G-N1i3FVuW2CtDiKPPS8SOWruhaNxEGhztcAPUmafjkAt2kqpnLXkRVypSdVD2VuIFZQe9H7Jgb3P4w3w2vGH8b1sBNMLetM6_ph3ACUbI1kZoy1-cTPj5QrlNghXCSu54PQ=w1024-h1280-no?authuser=0" width="512" /></a></center>
<p> </p>
<p>We decided to partner with <a href="https://foundation.app/ixshells">ixshells</a>, an artist from Panama to create this piece. Beyond creating a one-of-a-kind piece of art, she helped us to mobilize the NFT community and raise awareness about what we do for privacy online among many who had never heard of Tor.</p>
<h2>NFTs and climate change</h2>
<p>We wanted to be mindful of the impact of the blockchain on climate change. Part of our decision to move forward came after we looked into the efforts Ethereum has been putting forward to address their part in this—<a href="https://blog.ethereum.org/2021/05/18/country-power-no-more/">here is a blog post from them that just came out about moving ETH from PoW to PoS</a> and what this means for ETH’s climate impact.</p>
<p>We also decided that instead of buying carbon offsets as part of this auction, we would put money in the hands of those who are on the frontlines fighting for our planet. We chose to donate to the Munduruku Wakoborũn Women's Association, a grassroots indigenous organization in Pará, Brazil.</p>
<p>At the time of the auction, we decided to help them because illegal miners had attacked, burned, and destroyed their office. But more recently the home of their coordinator, Maria Leusa Kaba, was also <a href="https://movimentomundurukuiperegayuii.wordpress.com/2021/05/26/emergency-communique-from-the-munduruku-organizations-of-resistance/">burned and destroyed</a>. If others would like to support their work, <a href="https://movimentomundurukuiperegayuii.wordpress.com/2021/05/26/emergency-communique-from-the-munduruku-organizations-of-resistance/">you can find more information here</a>. The Munduruku Wakoborũn Women's Association is yet another example of the kinds of organizations and communities for whom we build our technology—people who need help to stay safe online in order to keep fighting for their rights. We invite others to help them as well.</p>
<h2>Results of the auction</h2>
<p>After roughly 24 hours of bidding, the NFT sold to the highest bidder, <a href="https://foundation.app/blog/pleasrdao">PleasrDAO</a>, a decentralized autonomous organization that also purchased Stay Free by Edward Snowden, for 2,224 ETH, which equated to roughly $5.5 million at the time of sale.</p>
<p>As a result of this auction, <a href="https://foundation.app/ixshells">ixshells</a> became the highest selling female NFT artist on Foundation. She was an excellent partner in this process, and we hope you check out the rest of her awesome generative artwork.</p>
<p>Some news coverage:</p>
<ul>
<li aria-level="1"><a href="https://beincrypto.com/tor-plans-to-enter-nft-market/">Tor’s Upcoming NFT Auction to Let Bidders Own Piece of History</a> (BeInCrypto)</li>
<li aria-level="1"><a href="https://www.bleepingcomputer.com/news/technology/tor-project-auctions-off-the-first-onion-url-ever-created-as-an-nft/">Tor Project auctions off the first Onion URL ever created as an NFT</a> (BleepingComputer)</li>
<li aria-level="1"><a href="https://decrypt.co/71017/tor-sells-nft-of-first-onion-url-for-2-million-in-eth-to-pleasrdao">Tor Sells NFT of First .onion URL For $2 Million in ETH to PleasrDAO</a> (Decrypt)</li>
<li aria-level="1"><a href="https://finance.yahoo.com/news/highest-selling-female-nft-artist-164000369.html">Highest Selling Female NFT Artist Itzel Yard Nets 500 ETH for ‘Dreaming at Dusk’</a> (Yahoo! Finance)</li>
</ul>
<h2>What’s next</h2>
<p>The funds raised from this auction will help us to:</p>
<ol>
<li aria-level="1"><strong>Continue work with grassroots communities in the Global South </strong>with training on privacy and security online and give a percentage of the proceeds to one of these organizations.</li>
<li aria-level="1"><strong>Improve the security of our network. </strong>Dusk might go, but v3 onion services are here to stay. These funds will help make onions more resilient against anonymity and DoS attacks.</li>
<li aria-level="1"><strong>Work on Arti, a rewrite of Tor in Rust</strong>, which improves Tor's security, makes it more sustainable, easier to improve, and lighter-weight for mobile integration.</li>
<li aria-level="1"><strong>Keep Tor strong for whistleblower solutions like SecureDrop and GlobaLeaks</strong>, used by sources to communicate about important stories with journalists without sacrificing their anonymity.</li>
</ol>
<p>And of course, save a bit of it for our future.</p>

---
_comments:

<a id="comment-291837"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291837" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 28, 2021</p>
    </div>
    <a href="#comment-291837">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291837" class="permalink" rel="bookmark">Scrolling down https:/…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Scrolling down <a href="https://foundation.app/ixshells" rel="nofollow">https://foundation.app/ixshells</a> in Tor Browser (Safer) leads to</p>
<blockquote><p>An unexpected error has occurred.</p></blockquote>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-291839"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291839" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 29, 2021</p>
    </div>
    <a href="#comment-291839">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291839" class="permalink" rel="bookmark">&gt; instead of buying carbon…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; instead of buying carbon offsets [...] We chose to donate to the Munduruku Wakoborũn Women's Association, a grassroots indigenous organization in Pará, Brazil.</p>
<p>That's great and all, but don't you think you guys should be spending the money on Tor and not re-donating it to random charities? </p>
<p>I say random because I can't find any direct connection between the beneficiary and carbon emissions or cryptocurrencies. Said illegal miners were conventional gold miners, not cryptocurrency miners. Yes, conventional mining has a carbon footprint just like most of the activities humankind engages in, but it's nowhere near that of cryptocurrency mining.</p>
<p>Again, it's very generous of you, but at the same time, it doesn't inspire a lot of confidence Tor Project's fundraisers and donation campaigns. When I donate to the Tor Project, I expect my money to be spent on Tor. In other words: if I wanted to help out Munduruku Wakoborũn, I'd donate to them myself.</p>
<p>BTW, congrats on raising an astonishing $2M!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-291841"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291841" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 29, 2021</p>
    </div>
    <a href="#comment-291841">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291841" class="permalink" rel="bookmark">This is great! I can&#039;t wait…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>This is great! I can't wait to see the effects of two million unrestricted dollars.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-291843"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291843" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Publius (not verified)</span> said:</p>
      <p class="date-time">June 01, 2021</p>
    </div>
    <a href="#comment-291843">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291843" class="permalink" rel="bookmark">I agree with the previous…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I agree with the previous comment that funds given to Tor should sustain The Tor Project, not other charities. The Tor Project needs funds, and going bankrupt because you're trying to help other charities means The Tor Project will not be able to help anyone at all. If I donate money to The Tor Project I expect it to fund The Tor Project.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-291850"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291850" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 02, 2021</p>
    </div>
    <a href="#comment-291850">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291850" class="permalink" rel="bookmark">I am a Tor user/contributor…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I am a Tor user/contributor who is very concerned with human rights, social justice, and environmental issues, so I am very glad that you are aware of the criticism that NFTs might be bad for the environment (if they become widely used).  That said, I am very glad that you obtained 2 million USD and I like the uses you mention!</p>
<p>On Southern outreach I agree that Amazonia should be high priority because this is one of the regions where indigenous/environmental activists (and the  global environment) are most endangered, but I also point to cartel murders and endemic corruption among "security authorities" in Mexico as a phenomenon where I hope Tor (and Tails) can possibly play a helpful role.  On the brighter side, currently there is hope of serious political reform in Chile, a country which already has a local Tor community, so I hope TP can stay in touch with activists there during this critical time in the political development of Chile.</p>
<p>Any chance the Arti rewrite of Tor in Rust could possibly enable a completely new version of secure messaging using the Signal protocol?   IMO an important issue to bear in mind as TP tries to enable citizens to use Tor products on their smart phones is that many activists lack enough secure income to purchase top of the line smart phones, so I hope TP will try to make "Tor for smart phone" and even "Signal for smart phone" usable (safely) on low end phones.  </p>
<p>I acknowledge that this will be challenging, because makers of low end phones and low end WiFi Telecoms like Cricket (owned by ATT) and TracFone (owned by Verizon) have absolutely horrendous multiple insecurities and seem to feel free to engage in particularly abusive practices which endanger their customers.  Recall for example that Cricket was stripping StartTLS from emails sent via Cricket phones even before being bought out by ATT (see the old EFF blog post), the US telecom with the longest history of close cooperation with NSA on various covert dragnet surveillance programs.  Further, TracFone was formerly owned by Carlos Slim, who could be called Latin America's analog of Bezos and Zuckerberg; Tracfone has also engaged in many abusive practices over the years.</p>
<p>I am a serial whistleblower ;-/ who has racked up a few successes over the years (we often fail to persuade journalists to write about leaks, but sometimes we are pleasantly surprised), so I have  good reason for being happy whenever I see maintenance, auditing, and development work being done to protect whistle-blowers.  So thanks for putting money towards supporting Secure Drop.</p>
<p>I would like to point out (again) that IMHO several troubling and widely reported phenomena provide an opportunity for Tor Project to reach out to journalists and US politicians:</p>
<p>o widespread adoption of such insecure products as Zoom for all manner of US federal/state/local and US school and US healthcare functions during the pandemic lockdown,</p>
<p>o state-sponsored cyberattacks (including supply chain attacks) on a critical software/hardware infrastructure widely used by US government agencies, US healthcare providers, and NGOs (especially healthcare and human rights orgs),</p>
<p>o ransomware attacks on US city government networks, health care providers, etc.</p>
<p>o state-sponsored cyberattacks on US power grid, pipeline infrastructure, and food production.</p>
<p>I believe TP should be pointing out the products like OnionShare, if the Tor network and Onionshare itself can be scaled up to handle greatly increased load, appear to offer much safer methods of sharing securely small files between USG agencies, between patients and healthcare providers, etc.  A typical example: years after unencrypted faxes were banned in the EU, it is still routine for sensitive medical information to be sent over unencrypted fax; these are often small files pertaining to a single client which would be better sent via OnionShare IMO.</p>
<p>See an op-ed by a terribly misguided RAND thinktanker published today in The Hill:</p>
<p>TheHill.com<br />
How COVID-19 lessons can transform US mental health care<br />
Ryan K. McBain, opinion contributor<br />
2 Jun 2021</p>
<p>What he (and Big Tech) is urging is exactly the wrong way to go at a time when endemic insecurities in US cyberthings are under such determined sophisticated and damaging attacks; c.f. the Vastaamo breach in Finland which shows the inevitable outcome in the  US if McBain and friends get their way.   Alas, big checks written by Big Tech lobbyists may outweigh input from TP, but I hope you will at last try to reach out to US politicians who will only hear disinformation FUD from FBI unless TP works with EFF and ACLU to counter the disastrously wrong-headed FBI messaging.</p>
<p>Further, I speculate that possibly university researchers should try to find some way to use OnionShare to send commands to remote power grid equipment over the Tor network, which might lead to a replacement for SCADA, years in the future.  I think this would be very hard but the payoff would be enormous.  Basically, it seems to me that USG is rushing to embrace insecure "newer" teletechnologies like Zoom while clinging to insecure old technologies like fax machines, when it should be embracing Tor network as something which USG and US critical infrastructure and US healthcare providers should all be using (with appropriate donations of many many fast Tor servers and whatever else is needed to handle the increased load).</p>
<p>Keep up the good work!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-291851"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291851" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 02, 2021</p>
    </div>
    <a href="#comment-291851">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291851" class="permalink" rel="bookmark">&gt; The Tor Project needs…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; The Tor Project needs funds</p>
<p>Yes, so please join me in sending in another unrestricted user contribution so that TP can rehire more of its laid off employees who can provide badly needed experience/expertise from Day One.</p>
<p>&gt; and going bankrupt </p>
<p>I hope that never happens, but Alsmith wrote:</p>
<p>&gt;&gt; we have made strong strides in regaining solid financial footing, much of which is a result of the 2020 year-end campaign (#UseAMaskUseTor) and your generous support during this time. We’ve also been able to re-hire several staff members and about a year on from that moment, we are in a better place.</p>
<p>So, "better place", not "going bankrupt", which is good to know, yes?</p>
<p>&gt; because you're trying to help other charities</p>
<p>As I understand alsmith's post, the entire two million USD raised from the auction is going to four specific Tor Project activities, but as part of the auction process, Ethereum offered to sell carbon offsets but TP decided to instead donate the money they could have spent on the offsets to a worthy environmental related charity instead, as a way of recognizing the concerns about the climate being harmed by NFTs (if this takes off in a huge way).  As I understand your criticism, you feel TP should not have done this.  I do not think the amount was anywhere close to two million USD but I guess alsmith can clarify that point.</p>
<p>Please note that alsmith wrote:</p>
<p>&gt;&gt; We decided to partner with ixshells, an artist from Panama to create this piece. Beyond creating a one-of-a-kind piece of art, she helped us to mobilize the NFT community and raise awareness about what we do for privacy online among many who had never heard of Tor.</p>
<p>Exactly!  I am one of the users who have urged TP to try to reach out to the kind of orgs which most need Tor but who may never even have heard of Tor, or who have been deterred by all the largely unopposed anti-Tor FUD coming from our enemy, US FBI.</p>
<p>I think all the decisions described in the blog are good ones, but I hope that when Tor users want to help out a particular Tor initiative, they will send a private donation to TP.  The amounts individual ordinary citizens can donate may be small but they add up fast.</p>
<p>Also, please note that Electronic Frontier Foundation (EFF.org) has been very successful in relying mostly on small user donations, and also have been very successful with their innovative use of NFT auctions.  Noone knows whether the second will continue to be a good source of funds for financially stressed NGOs, but I am one of the users who  have repeatedly urged Tor Project to imitate EFF's most successful fundraising and outreach methods.</p>
</div>
  </div>
</article>
<!-- Comment END -->
