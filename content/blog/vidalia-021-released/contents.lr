title: Vidalia 0.2.1 released
---
pub_date: 2009-08-12
---
author: phobos
---
tags:

vidalia release
usability enhancements
---
categories:

releases
usability
---
_html_body:

<p>Vidalia 0.2.1 is now available.  This is a test release of the 0.2.x branch, which we hope to soon make the mainline version; replacing the 0.1.x branch.</p>

<p>Vidalia can be downloaded at <a href="https://www.torproject.org/vidalia/" rel="nofollow">https://www.torproject.org/vidalia/</a>.</p>

<p>Changelog:</p>

<ul>
<li>Add a "Find Bridges Now" button that will attempt to automatically<br />
    download a set of bridge addresses and add them to the list of bridges<br />
    in the Network settings page.</li>
<li>Add support for building with Google's Breakpad crash reporting<br />
    library (currently disabled by default).</li>
<li>Show or hide the "Who has used my bridge recently?" link along with<br />
    the other bridge-related widgets when the user toggles the relay mode<br />
    in the Network settings page. (Ticket #480)</li>
<li>Tolerate bridge addresses that do not specify a port number, since Tor<br />
    now defaults to using port 443 in such cases.</li>
<li>Add support for viewing the map as a full screen widget when built<br />
    with KDE Marble support.</li>
<li>Compute the salted hash of the control password ourself when starting<br />
    Tor, rather than launching Tor once to hash the password, parsing the<br />
    output, and then again to actually start Tor.</li>
<li>Add a signal handler that allows Vidalia to clean up and exit normally<br />
    when it catches a SIGINT or SIGTERM signal. (Ticket #481)</li>
<li>If the user chooses to ignore further warnings for a particular port,<br />
    remove it from the WarnPlaintextPorts and RejectPlaintextPorts<br />
    settings immediately. Also remember their preferences and reapply them<br />
    later, even if Tor is unable to writes to its torrc.(Ticket #493)</li>
<li>Don't display additional plaintext port warning message boxes until<br />
    the first visible message box is dismissed. (Ticket #493)</li>
<li>Renamed the 'make win32-installer' CMake target to 'make dist-win32'<br />
    for consistency with our 'make dist-osx' target.</li>
<li>Fix a couple bugs in the WiX-based Windows installer related to building<br />
    a Marble-enabled Vidalia installer.</li>
<li>Write the list of source files containing translatable strings to a<br />
    .pro file and supply just the .pro file as an argument to lupdate, rather<br />
    than supplying all of the source file names themselves.</li>
</ul>

