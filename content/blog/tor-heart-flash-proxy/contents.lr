title: Tor at the Heart: Flash Proxy
---
pub_date: 2016-12-16
---
author: ssteele
---
tags:

heart of Internet freedom
flashproxy
cupcake
---
_html_body:

<p><i>During the month of December, we're highlighting other organizations and projects that rely on Tor, build on Tor, or are accomplishing their missions better because Tor exists. Check out our blog each day to learn about our fellow travelers. And please support the Tor Project! We're at the heart of Internet freedom.</i> <a href="https://torproject.org/donate/donate-blog16" rel="nofollow">Donate today</a>!</p>

<p><b>Flash Proxy</b></p>

<p>Sometimes Tor bridge relays can be blocked despite the fact that their addresses are handed out only a few at a time. <a href="https://crypto.stanford.edu/flashproxy/" rel="nofollow">Flash proxies</a> create many, generally ephemeral bridge IP addresses, with the goal of outpacing a censor's ability to block them. Rather than increasing the number of bridges at static addresses, flash proxies make existing bridges reachable by a larger and changing pool of addresses.</p>

<p>"Flash proxy" is a name that should make you think "quick" and "short-lived." Our implementation uses standard web technologies: JavaScript and WebSocket. (In the long-ago past we used Adobe Flash, but do not any longer.)</p>

<p>Flash Proxy is built into Tor Browser. In fact, any browser that runs JavaScript and has support for WebSockets is a potential proxy available to help censored Internet users.</p>

<p><b>How It Works</b></p>

<p>In addition to the Tor client and relay, we provide three new pieces. The Tor client contacts the flash proxy facilitator to advertise that it needs a connection. The facilitator is responsible for keeping track of clients and proxies, and assigning one to another. The flash proxy polls the facilitator for client registrations, then begins a connection to the client when it gets one. The transport plugins on the client and the relay broker the connection between WebSockets and plain TCP.</p>

<p>A sample session may go like this:</p>

<p>1.   The client starts Tor and the client transport plugin program (flashproxy-client), and sends a registration to the facilitator using a secure rendezvous. The client transport plugin begins listening for a remote connection.<br />
2.  A flash proxy comes online and polls the facilitator.<br />
3.  The facilitator returns a client registration, informing the flash proxy where to connect.<br />
4.  The proxy makes an outgoing connection to the client, which is received by the client's transport plugin.<br />
5.  The proxy makes an outgoing connection to the transport plugin on the Tor relay. The proxy begins sending and receiving data between the client and relay.</p>

<p>From the user's perspective, only a few things change compared to using normal Tor. The user must run the client transport plugin program and use a slightly modified Tor configuration file. </p>

<p><b>Cupcake</b></p>

<p><a href="https://github.com/glamrock/cupcake" rel="nofollow">Cupcake</a> is an easy way to distribute Flash Proxy, with the goal of getting as many people to become bridges as possible.</p>

<p>Cupcake can be distributed in two ways:</p>

<ul type="disc">
<li>As a Chrome or Firefox add-on (turning your computer into a less temporary proxy)</li>
<li>As a module/theme/app on popular web platforms (turning every visitor to your site into a temporary proxy)</li>
</ul>

---
_comments:

<a id="comment-225553"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-225553" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 16, 2016</p>
    </div>
    <a href="#comment-225553">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-225553" class="permalink" rel="bookmark">Would Snowflake make meek</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Would Snowflake make meek faster?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-225554"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-225554" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 16, 2016</p>
    </div>
    <a href="#comment-225554">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-225554" class="permalink" rel="bookmark">Heh, when I read the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Heh, when I read the headline I thought this must refer to protections for Adobe Flash--- as in on-line video--- but you explained what you really mean very clearly, and its awesome!</p>
<p>More innovation like this please!</p>
<p>You must be causing great dismay in authoritarian circles, and that's wonderful!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-225558"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-225558" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 16, 2016</p>
    </div>
    <a href="#comment-225558">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-225558" class="permalink" rel="bookmark">What about Tor Flashproxy</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What about Tor Flashproxy Badge?</p>
<p><a href="https://addons.mozilla.org/EN-US/firefox/addon/tor-flashproxy-badge/" rel="nofollow">https://addons.mozilla.org/EN-US/firefox/addon/tor-flashproxy-badge/</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-225563"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-225563" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 16, 2016</p>
    </div>
    <a href="#comment-225563">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-225563" class="permalink" rel="bookmark">but i cannot add the addon</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>but i cannot add the addon on tbb ! so i cannot try this flash proxy (i never heard about that) in short i do not understand what it is could you show us an example a swf or something clearer ?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-225596"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-225596" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 16, 2016</p>
    </div>
    <a href="#comment-225596">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-225596" class="permalink" rel="bookmark">&gt; &quot;Flash proxy&quot; is a name</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; "Flash proxy" is a name that should make you think "quick" and "short-lived." Our implementation uses standard web technologies: JavaScript and WebSocket. (In the long-ago past we used Adobe Flash, but do not any longer.)</p>
<p>What a brilliant wordplay on an unlikely coincidence. I love it!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-225607"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-225607" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 16, 2016</p>
    </div>
    <a href="#comment-225607">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-225607" class="permalink" rel="bookmark">Washnt flash proxy</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Washnt flash proxy functionality removed from the Tor Browser Bundle a year ago?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-225614"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-225614" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 16, 2016</p>
    </div>
    <a href="#comment-225614">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-225614" class="permalink" rel="bookmark">Wow, I am learning a lot</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Wow, I am learning a lot from these blogs. Thanks for all the good work!</p>
<p>A happy contributor.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-225735"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-225735" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 17, 2016</p>
    </div>
    <a href="#comment-225735">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-225735" class="permalink" rel="bookmark">It appears to me that the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It appears to me that the facilitator is the long-lived component. Could censors not then block clients' ability to connect to the facilitators the same way they block bridges?</p>
<p>Moreover, what exactly is the facilitator? A binary that listens for WebSocket connections? And are they run by volunteers like bridges?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-226188"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-226188" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 18, 2016</p>
    </div>
    <a href="#comment-226188">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-226188" class="permalink" rel="bookmark">I added it to my browser!!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I added it to my browser!! This is my first contribution to Tor project (so smaller than what you did to you...) Really appreciate your help!</p>
</div>
  </div>
</article>
<!-- Comment END -->
