title: Tor Browser 7.5a10 is released
---
pub_date: 2017-12-20
---
author: boklm
---
tags:

tor browser
tbb
tbb-7.5
---
categories: applications
---
_html_body:

<p>Tor Browser 7.5a10 is now available from the <a href="https://www.torproject.org/projects/torbrowser.html.en#downloads-alpha">Tor Browser Project page</a> and also from our <a href="https://www.torproject.org/dist/torbrowser/7.5a10/">distribution directory</a>.</p>
<p>This release updates Tor to <a href="https://blog.torproject.org/tor-0327-rc-released">0.3.2.7-rc</a> and OpenSSL to 1.0.2n. The security slider has been updated, <a href="https://trac.torproject.org/projects/tor/ticket/21847">following the experience provided for mobile users</a>. On Linux, the "Print to File" feature should be working again.</p>
<p>The full <a href="https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt">changelog</a> since Tor Browser 7.5a9 is:</p>
<ul>
<li>All Platforms
<ul>
<li>Update Tor to 0.3.2.7-rc</li>
<li>Update OpenSSL to 1.0.2n</li>
<li>Update Torbutton to 1.9.8.4
<ul>
<li><a href="https://trac.torproject.org/projects/tor/ticket/21847">Bug 21847</a>: Update copy for security slider</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/10573">Bug 10573</a>: Replace deprecated nsILocalFile with nsIFile (code clean-up)</li>
<li>Translations update</li>
</ul>
</li>
<li>Update Tor Launcher to 0.2.14.2
<ul>
<li><a href="https://trac.torproject.org/projects/tor/ticket/24623">Bug 24623</a>: Revise "country that censors Tor" text</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/24428">Bug 24428</a>: Bootstrap error message sometimes lost</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/24624">Bug 24624</a>: tbb-logo.svg may cause network access</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/10573">Bug 10573</a>: Replace deprecated nsILocalFile with nsIFile (code clean-up)</li>
<li>Translations update</li>
</ul>
</li>
<li>Update NoScript to 5.1.8.3</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/23104">Bug 23104</a>: CSS line-height reveals the platform Tor Browser is running on</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/24398">Bug 24398</a>: Plugin-container process exhausts memory</li>
</ul>
</li>
<li>OS X
<ul>
<li><a href="https://trac.torproject.org/projects/tor/ticket/24566">Bug 24566</a>: Avoid white flashes when opening dialogs in Tor Browser</li>
</ul>
</li>
<li>Linux
<ul>
<li><a href="https://trac.torproject.org/projects/tor/ticket/23970">Bug 23970</a>: Make "Print to File" work with sandboxing enabled</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/23016">Bug 23016</a>: "Print to File" is broken on some non-english Linux systems</li>
</ul>
</li>
<li>Android
<ul>
<li><a href="https://trac.torproject.org/projects/tor/ticket/22084">Bug 22084</a>: Spoof network information API</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-273157"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273157" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 20, 2017</p>
    </div>
    <a href="#comment-273157">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273157" class="permalink" rel="bookmark">Thanks for another great…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks for another great release. What happened to Mike Perry on the TBB team?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-273158"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273158" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">December 20, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-273157" class="permalink" rel="bookmark">Thanks for another great…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-273158">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273158" class="permalink" rel="bookmark">He engineered his way out of…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>He engineered his way out of the team and is working on Tor core things again now.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-273160"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273160" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span content="Thank you for the hard work!">Thank you for … (not verified)</span> said:</p>
      <p class="date-time">December 20, 2017</p>
    </div>
    <a href="#comment-273160">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273160" class="permalink" rel="bookmark">I appreciate TOR and use it…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I appreciate TOR and use it daily.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273164"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273164" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 20, 2017</p>
    </div>
    <a href="#comment-273164">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273164" class="permalink" rel="bookmark">14:25:29.848 Error: Callback…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>14:25:29.848 Error: Callback received for bad URI: [xpconnect wrapped nsIURI] 1 permissions.js:231:11<br />
    onIndexedDBUsageCallback chrome://browser/content/pageinfo/permissions.js:231:11</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273166"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273166" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 20, 2017</p>
    </div>
    <a href="#comment-273166">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273166" class="permalink" rel="bookmark">Update Failed…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><strong>Update Failed</strong><br />
The partial Update could not be applied. Tor Browser will try again by downloading a complete Update.</p>
<p>Is this normal???</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-273169"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273169" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  boklm
  </article>
    <div class="comment-header">
      <p class="comment__submitted">boklm said:</p>
      <p class="date-time">December 20, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-273166" class="permalink" rel="bookmark">Update Failed…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-273169">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273169" class="permalink" rel="bookmark">This is not normal. Did the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>This is not normal. Did the full update work?</p>
<p>One possible reason for an error with the partial update could be that you modified a file shipped by Tor Browser.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-273170"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273170" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 20, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to boklm</p>
    <a href="#comment-273170">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273170" class="permalink" rel="bookmark">The full update worked…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The full update worked although took a while of course. I did modify the tor that was shipped from 0.3.2.6a to 0.3.2.7a, so that could be it, thanks!</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-273171"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273171" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>unfit (not verified)</span> said:</p>
      <p class="date-time">December 20, 2017</p>
    </div>
    <a href="#comment-273171">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273171" class="permalink" rel="bookmark">&#039;The Rise of ``Worse is…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>'The Rise of ``Worse is Better''' by Richard Gabriel. (25 years ago)<br />
<a href="https://en.wikipedia.org/wiki/Worse_is_better" rel="nofollow">https://en.wikipedia.org/wiki/Worse_is_better</a><br />
 Il meglio è nemico del bene is a less absurd/idiot version.<br />
<a href="https://en.wikipedia.org/wiki/Perfect_is_the_enemy_of_good" rel="nofollow">https://en.wikipedia.org/wiki/Perfect_is_the_enemy_of_good</a> (400 years ago)</p>
<p>1° mit is not a reference.<br />
2° an obsolete way of life showed like a model of style/design is a demonstration of an inapt mind.<br />
3° Better crypto (replaced SHA1/DH/RSA1024 with SHA3/ed25519/curve25519)<br />
<a href="https://trac.torproject.org/projects/tor/wiki/doc/NextGenOnions#Howtoconnecttothetesthubfornextgenonionservices" rel="nofollow">https://trac.torproject.org/projects/tor/wiki/doc/NextGenOnions#Howtoco…</a></p>
<p>It does not solve our tor user problem , wait 5_10 years before running a modern tor version is not a good *enough attitude.</p>
<p>Does an onion grow up on the same ground than a rose ?<br />
<a href="https://blog.torproject.org/tor-browser-75a9-released" rel="nofollow">https://blog.torproject.org/tor-browser-75a9-released</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273174"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273174" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 20, 2017</p>
    </div>
    <a href="#comment-273174">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273174" class="permalink" rel="bookmark">Tor NOTICE: Switching to…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor NOTICE: Switching to guard context "default" (was using "bridges")<br />
Tor WARN: Could not choose valid address for OzDqSJWvi2NFpDubvxp<br />
Tor WARN: Failed to find node for hop #1 of our path. Discarding this circuit.<br />
Tor WARN: Could not choose valid address for OzDqSJWvi2NFpDubvxp<br />
Tor WARN: Failed to find node for hop #1 of our path. Discarding this circuit.<br />
... indefinite number of times ...<br />
switching from obfs4 to `firewalled` guard option fucked up as expected :-)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273181"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273181" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>null (not verified)</span> said:</p>
      <p class="date-time">December 22, 2017</p>
    </div>
    <a href="#comment-273181">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273181" class="permalink" rel="bookmark">&gt;Update Torbutton to 1.9.8.4…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt;Update Torbutton to 1.9.8.4<br />
I see what you did there.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273187"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273187" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 22, 2017</p>
    </div>
    <a href="#comment-273187">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273187" class="permalink" rel="bookmark">Last night my Tor connection…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Last night my Tor connection broke. The Tor Arm console had an error message saying something like my "Guard node has already failed more than 700 circuits, possibly doing some routing attack". I didn't save the Guard node ID.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273201"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273201" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>JackAHole (not verified)</span> said:</p>
      <p class="date-time">December 23, 2017</p>
    </div>
    <a href="#comment-273201">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273201" class="permalink" rel="bookmark">Sorry to tell you this…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Sorry to tell you this... but updates this often are a major security risk</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273210"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273210" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 24, 2017</p>
    </div>
    <a href="#comment-273210">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273210" class="permalink" rel="bookmark">Audio, that I needed for…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Audio, that I needed for things like message notification, does not work at all, no sound at all, ever since the switch to pulseaudio. I have pulseaudio installed and working, that isn't the problem.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273212"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273212" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 24, 2017</p>
    </div>
    <a href="#comment-273212">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273212" class="permalink" rel="bookmark">16:40:51.869 uri is…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>16:40:51.869 uri is undefined 1 MatchPattern.jsm:95<br />
    matches resource://gre/modules/MatchPattern.jsm:95:1<br />
    matches/&lt; resource://gre/modules/MatchPattern.jsm:118:42<br />
    some self-hosted:208:1<br />
    matches resource://gre/modules/MatchPattern.jsm:118:12<br />
    urlMatches resource://gre/modules/WebRequestCommon.jsm:55:12<br />
    shouldRunListener resource://gre/modules/WebRequest.jsm:549:12<br />
    runChannelListener resource://gre/modules/WebRequest.jsm:692:14<br />
    errorCheck resource://gre/modules/WebRequest.jsm:568:7<br />
    runChannelListener resource://gre/modules/WebRequest.jsm:676:20<br />
    onStopRequest resource://gre/modules/WebRequest.jsm:841:5<br />
    onStopRequest resource://gre/modules/WebRequest.jsm:351:5</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273227"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273227" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span content="Anonymous (non verifiable)">Anonymous (non… (not verified)</span> said:</p>
      <p class="date-time">December 27, 2017</p>
    </div>
    <a href="#comment-273227">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273227" class="permalink" rel="bookmark">Great release as ever!…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Great release as ever!<br />
When will the project switch to FF 57+ ?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273258"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273258" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 30, 2017</p>
    </div>
    <a href="#comment-273258">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273258" class="permalink" rel="bookmark">New install of 7.5a10 on…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>New install of 7.5a10 on Windows 7.  Out of the box settings fail Panopticlick fingerprint test.  Unique fingerprint.  Moving security settings up to medium same result.  Turning off almost all settings in NoScript make test non-functional so unsure if still unique fingerprint.  </p>
<p>No HTTPS Everywhere.  Shows as enabled in Menu &gt; Addons but no button visible on browser as in earlier versions.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273270"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273270" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 01, 2018</p>
    </div>
    <a href="#comment-273270">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273270" class="permalink" rel="bookmark">Fucking ukrainian exits…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Fucking ukrainian exits block Russian sites.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273326"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273326" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>john fillmore (not verified)</span> said:</p>
      <p class="date-time">January 07, 2018</p>
    </div>
    <a href="#comment-273326">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273326" class="permalink" rel="bookmark">need to download 5.7 foe osx…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>need to download 5.7 foe osx 10.6 cant update computer to new system so must have tor 5.0 or im sh%t out of luck can fid a link that dont send me to 7.1 or higher</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273367"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273367" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 10, 2018</p>
    </div>
    <a href="#comment-273367">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273367" class="permalink" rel="bookmark">I&#039;m running a 64-bit Linux…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I'm running a 64-bit Linux distro, and I'm confused.<br />
Should I be using this (Tor Browser 7.5a10), or 7.0.11 at<br />
<a href="https://www.torproject.org/download/download-easy.html.en#linux" rel="nofollow">https://www.torproject.org/download/download-easy.html.en#linux</a>, and why?<br />
How do the two differ?<br />
Would someone who knows what they're writing about please explain?<br />
Thanks in advance.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-273397"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273397" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>DPL (not verified)</span> said:</p>
      <p class="date-time">January 13, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-273367" class="permalink" rel="bookmark">I&#039;m running a 64-bit Linux…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-273397">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273397" class="permalink" rel="bookmark">I&#039;m running macOS Sierra and…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I'm running macOS Sierra and I use both versions. I think that 7.5a10 is an alpha that runs some experimental features...(I'm guessing it's the same for Linux, although Linux still has a hardened version if I'm not mistaken. It's been awhile since I looked at the downloads available). and 7.0.11 is the latest stable release.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273423"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273423" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">January 15, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-273367" class="permalink" rel="bookmark">I&#039;m running a 64-bit Linux…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-273423">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273423" class="permalink" rel="bookmark">They differ in the sense…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>They differ in the sense that the version with the "a" in its version string (the alpha) usually contains patches that need to get tested in a wider audience across all the platforms we support. This can be new bug fixes to the browser part, a new toolchain or a new build environment etc. Thus, it is hard to write about specific changes between alpha and stable series. One of the changes between 7.5a10 and 7.0.11 is the Tor version shipped. The alpha release contains the latest Tor alpha version while the stable sticks to the latest stable Tor version.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-273447"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273447" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 18, 2018</p>
    </div>
    <a href="#comment-273447">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273447" class="permalink" rel="bookmark">When you set a certain…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>When you set a certain window size a warning pops up.<br />
How do you prevent the browser from telling its window size ?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273465"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273465" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>LINUX LOVER (not verified)</span> said:</p>
      <p class="date-time">January 20, 2018</p>
    </div>
    <a href="#comment-273465">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273465" class="permalink" rel="bookmark">When New TOR 7.12 and new…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>When New TOR 7.12 and new Tail 3.5 arrives. Tail 3.4 has a lot of issues. We appreciate speed up the operations. Why Tail 3.4 is still TOR 7.10 and does not update to 7.11 so on. Please make Tail in a way at least TOR can be updated .</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273475"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273475" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>new (not verified)</span> said:</p>
      <p class="date-time">January 20, 2018</p>
    </div>
    <a href="#comment-273475">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273475" class="permalink" rel="bookmark">FF 52.6.0  and 58.0 is out,…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>FF 52.6.0  and 58.0 is out, no releasenotes yet.<br />
New TBB release is necessary?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273893"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273893" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Pablo Gonzales (not verified)</span> said:</p>
      <p class="date-time">February 05, 2018</p>
    </div>
    <a href="#comment-273893">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273893" class="permalink" rel="bookmark">Thank you for Tor…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you for Tor.<br />
Guys, you are legends...<br />
Stay safe, Pablo G</p>
</div>
  </div>
</article>
<!-- Comment END -->
