title: December 2010 Progress Report
---
pub_date: 2011-01-11
---
author: phobos
---
tags:

progress report
new packages
scalability
new releases
---
categories:

releases
reports
---
_html_body:

<p><strong> New Releases </strong></p>

<ul>
<li>
On December 14, we released updated Tor Browser Bundles for Windows, OSX, and<br />
Linux, <a href="https://blog.torproject.org/blog/new-tor-browser-bundle-packages-0" rel="nofollow">https://blog.torproject.org/blog/new-tor-browser-bundle-packages-0</a></li>
<li>On December 17th, we released an updated -stable version of Tor, 0.2.1.28.<br />
Tor 0.2.1.28 does some code cleanup to reduce the risk of remotely exploitable<br />
bugs. Thanks to Willem Pinckaers for notifying us of the issue. The Common<br />
Vulnerabilities and Exposures project has assigned CVE-2010-1676 to this issue.<br />
We also took this opportunity to change the IP address for one of our directory<br />
authorities, and to update the geoip database we ship.<br />
<a href="https://blog.torproject.org/blog/tor-02128-released-security-patches" rel="nofollow">https://blog.torproject.org/blog/tor-02128-released-security-patches</a></li>
<li>On December 17th, we released an updated -alpha version of Tor,<br />
0.2.2.20-alpha.   Tor 0.2.2.20-alpha does some code cleanup to reduce the risk of remotely exploitable bugs. We also fix a variety of other significant bugs, change the IP address for one of our directory authorities, and update the minimum version<br />
that Tor relays must run to join the network. <a href="https://blog.torproject.org/blog/tor-02220-alpha-out-security-patches" rel="nofollow">https://blog.torproject.org/blog/tor-02220-alpha-out-security-patches</a></li>
</ul>

<p><strong>Enhancements that make Tor a better tool for users in censored<br />
countries.</strong></p>

<ul>
<li>Mike spent the past month and a half primarily working on preparing<br />
Torbutton for Firefox 4. This was a rather difficult task, as a lot has changed<br />
in this release, and the Javascript debugger doesn't yet support Firefox 4. The<br />
new mechanism works just fine for replacing XPCOM components. He also took the<br />
opportunity to clean up the code a bit. Firefox 3.5 and Firefox 4 both added<br />
some new APIs that make our job easier. We no longer rely so heavily on<br />
reimplementing pieces of Firefox using XPCOM re-registration. In fact, the only<br />
component we still need to hook is the external app launcher, to provide our<br />
warning message.</li>
<li>Mike reviewed a proposed Chrome API at: <a href="https://groups.google.com/a/chromium.org/group/chromium-dev/browse_thread/thread/4c318fb01062678a/89a11a7cbaa48d5f" rel="nofollow">https://groups.google.com/a/chromium.org/group/chromium-dev/browse_thre…</a>. The main goal there was to try to steer them away from declarative models and more towards blocking callback APIs that will work better for us. We'll see if it works.</li>
</ul>

<p><strong>Blocking Resistance</strong></p>

<p>Steven worked on plans of how to make Tor look more like other protocols, for<br />
when Tor's HTTPS-like cloaking is broken or someone blocks Tor along with HTTPS.<br />
The proposal is to allow Tor bridges to be configured to use one or more plugins<br />
which offer translation between Tor and obfuscated-Tor. There is now a proposal<br />
draft here:</p>

<p><a href="https://gitweb.torproject.org/tor.git/blob/HEAD:/doc/spec/proposals/ideas/xxx-pluggable-transport.txt" rel="nofollow">https://gitweb.torproject.org/tor.git/blob/HEAD:/doc/spec/proposals/ide…</a>.  Steven wrote a first proof-of-concept (albeit not compliant with the proposal), for a HTTP-like protocol, and put the code here: <a href="https://gitweb.torproject.org/sjm217/pluggable-transport.git" rel="nofollow">https://gitweb.torproject.org/sjm217/pluggable-transport.git</a>. A screenshot of the a Wireshark dump of Steven successfully accessing check.torproject.org is at:  <a href="http://www.cl.cam.ac.uk/~sjm217/volatile/pluggable-transport.png" rel="nofollow">http://www.cl.cam.ac.uk/~sjm217/volatile/pluggable-transport.png</a>.</p>

<p><strong>Outreach and Advocacy</strong></p>

<ul>
<li>Karen spoke at the Reykjavík Digital Freedom Conference, <a href="http://www.fsfi.is/radstefna2010/" rel="nofollow">http://www.fsfi.is/radstefna2010/</a>.</li>
<li> Erinn spoke at Moscow State University, <a href="http://www.linux.org.ru/news/security/5624240" rel="nofollow">http://www.linux.org.ru/news/security/5624240</a>.</li>
<li>Many tor people attended the CCC's 27C3,<br />
<a href="http://events.ccc.de/congress/2010/wiki/Main_Page" rel="nofollow">http://events.ccc.de/congress/2010/wiki/Main_Page</a>.</li>
<li>Karen participated in ``Hivos Digital Natives With a Cause?'' Thinkathon,<br />
<a href="http://www.hivos.net/Hivos-Knowledge-Programme/Themes/Digital-Natives-with-a-Cause/Publications/Digital-Natives-with-a-Cause-Thinkathon-Position-Papers" rel="nofollow">http://www.hivos.net/Hivos-Knowledge-Programme/Themes/Digital-Natives-w…</a>.</li>
</ul>

<p><strong>Bridge relay and bridge authority work</strong><br />
Jacob and Runa continue work on making it easy for people to become Tor bridge<br />
relays by default.  The Torouter project is very much an alpha code quality project, but making progress on OpenWRT-based wireless access points and the Excito B3 hardware.  The project page is kept up-to-date at <a href="https://trac.torproject.org/projects/tor/wiki/TheOnionRouter/Torouter" rel="nofollow">https://trac.torproject.org/projects/tor/wiki/TheOnionRouter/Torouter</a>.</p>

<p><strong>Scalability</strong></p>

<ul>
<li>Karsten wrote a technical report with an "Overview of Statistical Data in the Tor Network".  The idea is to give this report to interested researchers who want to know what statistical data we have. The report is available at <a href="https://metrics.torproject.org/papers/data-2010-12-29.pdf" rel="nofollow">https://metrics.torproject.org/papers/data-2010-12-29.pdf</a>.</li>
<li>Erinn and Sebastian worked on some Thandy &amp; Hudson hacking accomplishing:<br />
1) Hudson and Windows finally cooperate, so now we can have a Windows autobuilder; and 2) setup a basic instance of Thandy-the secure software update platform.</li>
<li>Mike did a quick modification to the TorFlow statsplitter.py script to have it output the results from the extra-info descriptors to give us breakdowns on port stats in more readable percentages, to compare the default exit policy of blutmagie to Amunet. It looks like the default exit policy causes you to write a heck of a lot more data, and over half of the read data is misc ports. Using these stats to produce new consensus weights to account for this seems like a good task to do.</li>
</ul>

<p><strong>Translations</strong><br />
We fully migrated from our own Pootle-based translation system to Transifex, <a href="https://www.transifex.net/projects/p/torproject/" rel="nofollow">https://www.transifex.net/projects/p/torproject/</a>.  A number of translations for various products have already come through for German, Arabic, Burmese, Simplified Chinese, Dutch, Finnish, French, Indonesian, Italian, Norwegian, Persian, Polish, and Romanian.</p>

