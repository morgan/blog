title: Tails 0.23 is out
---
pub_date: 2014-03-19
---
author: tails
---
tags:

tails
anonymous operating system
tails releases
---
categories:

partners
releases
---
_html_body:

<p>Tails, The Amnesic Incognito Live System, version 0.23, is out.</p>

<p>All users must upgrade as soon as possible: this release fixes <a href="https://tails.boum.org/security/Numerous_security_holes_in_0.22.1/index.en.html" rel="nofollow">numerous security issues</a>.</p>

<p><strong>Changes</strong></p>

<p>Notable user-visible changes include:</p>

<ul>
<li>Security fixes
<ul>
<li>Upgrade the web browser to 24.4.0esr-0+tails1~bpo60+1 (Firefox<br />
24.4.0esr + Iceweasel patches + Torbrowser patches).</li>
</ul>
</li>
<li>Major new features
<ul>
<li><a href="https://tails.boum.org/doc/first_steps/startup_options/mac_spoofing/index.en.html" rel="nofollow">Spoof the network interfaces' MAC address</a><br />
by default. It can be disabled in Tails Greeter.</li>
<li>Rework the way to<br />
<a href="https://tails.boum.org/doc/first_steps/startup_options/network_configuration/index.en.html" rel="nofollow">configure how Tor connects to the network</a><br />
by using bridges, proxies and restrictive firewalls. This option<br />
can be set from Tails Greeter, and replaces the old<br />
experimental "bridge mode" feature.</li>
</ul>
</li>
<li>Bugfixes
<ul>
<li>Additional software: do not crash when persistence is disabled.</li>
<li>Upgrade Pidgin to 2.10.9, that fixes some regressions introduced in the 2.10.8 security update.</li>
<li>Wait for Tor to have fully bootstrapped, plus a bit more time, before checking for upgrades and unfixed known security issues.</li>
<li>Disable the Intel Management Engine Interface driver. We don't need it in Tails, it might be dangerous, and it causes bugs on various hardware such as systems that reboot when asked to shut down.</li>
<li>Add a launcher for the Tails documentation. This makes it available in Windows Camouflage mode.</li>
<li>Remove the obsolete wikileaks.de account from Pidgin.</li>
</ul>
</li>
<li>Minor improvements
<ul>
<li>Upgrade Tor to 0.2.4.21-1~d60.squeeze+1.</li>
<li>Upgrade obfsproxy to 0.2.6-2~~squeeze+1.</li>
<li>Upgrade I2P to 0.9.11-1deb6u1.</li>
<li>Install 64-bit kernel instead of the 686-pae one. This is a necessary first step towards UEFI boot support.</li>
<li>Install Monkeysign (in a not-so-functional shape yet).</li>
<li>Disable the autologin text consoles. This was one of the blockers before a screen saver can be installed in a meaningful way.</li>
<li>Don't localize the text consoles anymore: it is broken on Wheezy, the intended users can as well use loadkeys, and we now do not have to trust setupcon to be safe for being run as root by the desktop user.</li>
<li>Make it possible to manually start IBus.</li>
<li>Reintroduce the possibility to switch identities in the Tor Browser, using a filtering proxy in front of the Tor ControlPort to avoid giving full control over Tor to the desktop user.</li>
<li>Incremental upgrades improvements:
<ul>
<li>Drop the Tails Upgrader launcher, to limit users' confusion.</li>
<li>Lock down sudo credentials a bit.</li>
<li>Hide debugging information.</li>
<li>Include ~/.xsession-errors in WhisperBack bug reports. This captures the Tails Upgrader errors and debugging information.</li>
<li>Report more precisely why an incremental upgrade cannot be done.</li>
<li>Various user interface and phrasing improvements.</li>
</ul>
</li>
<li>Don't install the Cookie Monster browser extension.</li>
<li>Add a browser bookmark pointing to Tor's Stack Exchange.</li>
<li>Remove the preconfigured #tor channel from the Pidgin: apparently, too many Tails users go ask Tails questions there, without making it clear that they are running Tails, hence creating a user-support nightmare.</li>
<li>Use (most of) Tor Browser's mozconfig.</li>
<li>Rebase the browser on top of iceweasel 24.3.0esr-1, to get the certificate authorities added by Debian back.</li>
<li>Give access to the relevant documentation pages from Tails Greeter.</li>
<li>Hide Tails Greeter's password mismatch warning when entry is changed.</li>
<li>Persistent Volume Assistant:
<ul>
<li>Take into account our installer is now called Tails Installer.</li>
<li>Optimize window height.</li>
<li>Display device paths in a more user-friendly way.</li>
</ul>
</li>
</ul>
</li>
</ul>

<p>See the <a href="https://git-tails.immerda.ch/tails/plain/debian/changelog" rel="nofollow">online Changelog</a> for technical details.</p>

<p><strong>Known issues</strong></p>

<ul>
<li><a href="https://tails.boum.org/support/known_issues/index.en.html" rel="nofollow">Longstanding</a> known issues.</li>
</ul>

<p><strong>I want to try it or to upgrade!</strong></p>

<p>Go to the <a href="https://tails.boum.org/download/index.en.html" rel="nofollow">download</a> page.</p>

<p>As no software is ever perfect, we maintain a list of <a href="https://tails.boum.org/support/known_issues/index.en.html" rel="nofollow">problems that affects the last release of Tails</a>.</p>

<p><strong>What's coming up?</strong></p>

<p>The next Tails release is <a href="https://tails.boum.org/contribute/calendar/" rel="nofollow">scheduled</a> for April 29.</p>

<p>Have a look to our <a href="https://labs.riseup.net/code/projects/tails/roadmap" rel="nofollow">roadmap</a> to see where we are heading to.</p>

<p>Would you want to help? There are many ways <a href="https://tails.boum.org/contribute/index.en.html" rel="nofollow"><strong>you</strong> can contribute to Tails</a>. If you want to help, come talk to us!</p>

<p><strong>Support and feedback</strong></p>

<p>For support and feedback, visit the <a href="https://tails.boum.org/support/" rel="nofollow">Support section</a> on the Tails website.</p>

