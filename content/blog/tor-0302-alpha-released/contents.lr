title: Tor 0.3.0.2-alpha is released
---
pub_date: 2017-01-23
---
author: arma
---
tags:

tor
alpha
release
---
categories:

network
releases
---
_html_body:

<p>Tor 0.3.0.2-alpha fixes a denial-of-service bug where an attacker could cause relays and clients to crash, even if they were not built with the --enable-expensive-hardening option. This bug affects all 0.2.9.x versions, and also affects 0.3.0.1-alpha: all relays running an affected version should upgrade.<br />
Tor 0.3.0.2-alpha also improves how exit relays and clients handle DNS time-to-live values, makes directory authorities enforce the 1-to-1 mapping of relay RSA identity keys to ED25519 identity keys, fixes a client-side onion service reachability bug, does better at selecting the set of fallback directories, and more.</p>

<p>You can download the source code from <a href="https://dist.torproject.org/" rel="nofollow">https://dist.torproject.org/</a> but most users should wait for the upcoming 7.0a Tor Browser alpha release, or for their upcoming system package updates.</p>

<h2>Changes in version 0.3.0.2-alpha - 2017-01-23</h2>

<ul>
<li>Major bugfixes (security, also in 0.2.9.9):
<ul>
<li>Downgrade the "-ftrapv" option from "always on" to "only on when --enable-expensive-hardening is provided."  This hardening option, like others, can turn survivable bugs into crashes--and having it on by default made a (relatively harmless) integer overflow bug into a denial-of-service bug. Fixes bug <a href="https://bugs.torproject.org/21278" rel="nofollow">21278</a> (TROVE-2017-001); bugfix on 0.2.9.1-alpha.
  </li>
</ul>
</li>
<li>Major features (security):
<ul>
<li>Change the algorithm used to decide DNS TTLs on client and server side, to better resist DNS-based correlation attacks like the DefecTor attack of Greschbach, Pulls, Roberts, Winter, and Feamster. Now relays only return one of two possible DNS TTL values, and clients are willing to believe DNS TTL values up to 3 hours long. Closes ticket <a href="https://bugs.torproject.org/19769" rel="nofollow">19769</a>.
  </li>
</ul>
</li>
</ul>

<p> </p>

<ul>
<li>Major features (directory authority, security):
<ul>
<li>The default for AuthDirPinKeys is now 1: directory authorities will reject relays where the RSA identity key matches a previously seen value, but the Ed25519 key has changed. Closes ticket <a href="https://bugs.torproject.org/18319" rel="nofollow">18319</a>.
  </li>
</ul>
</li>
<li>Major bugfixes (client, guard, crash):
<ul>
<li>In circuit_get_global_origin_list(), return the actual list of origin circuits. The previous version of this code returned the list of all the circuits, and could have caused strange bugs, including possible crashes. Fixes bug <a href="https://bugs.torproject.org/21118" rel="nofollow">21118</a>; bugfix on 0.3.0.1-alpha.
  </li>
</ul>
</li>
<li>Major bugfixes (client, onion service, also in 0.2.9.9):
<ul>
<li>Fix a client-side onion service reachability bug, where multiple socks requests to an onion service (or a single slow request) could cause us to mistakenly mark some of the service's introduction points as failed, and we cache that failure so eventually we run out and can't reach the service. Also resolves a mysterious "Remote server sent bogus reason code 65021" log warning. The bug was introduced in ticket <a href="https://bugs.torproject.org/17218" rel="nofollow">17218</a>, where we tried to remember the circuit end reason as a uint16_t, which mangled negative values. Partially fixes bug <a href="https://bugs.torproject.org/21056" rel="nofollow">21056</a> and fixes bug <a href="https://bugs.torproject.org/20307" rel="nofollow">20307</a>; bugfix on 0.2.8.1-alpha.
  </li>
</ul>
</li>
<li>Major bugfixes (DNS):
<ul>
<li>Fix a bug that prevented exit nodes from caching DNS records for more than 60 seconds. Fixes bug <a href="https://bugs.torproject.org/19025" rel="nofollow">19025</a>; bugfix on 0.2.4.7-alpha.
  </li>
</ul>
</li>
<li>Minor features (controller):
<ul>
<li>Add "GETINFO sr/current" and "GETINFO sr/previous" keys, to expose shared-random values to the controller. Closes ticket <a href="https://bugs.torproject.org/19925" rel="nofollow">19925</a>.
  </li>
</ul>
</li>
<li>Minor features (entry guards):
<ul>
<li>Add UseEntryGuards to TEST_OPTIONS_DEFAULT_VALUES in order to not break regression tests.
  </li>
<li>Require UseEntryGuards when UseBridges is set, in order to make sure bridges aren't bypassed. Resolves ticket <a href="https://bugs.torproject.org/20502" rel="nofollow">20502</a>.
  </li>
</ul>
</li>
<li>Minor features (fallback directories):
<ul>
<li>Select 200 fallback directories for each release. Closes ticket <a href="https://bugs.torproject.org/20881" rel="nofollow">20881</a>.
  </li>
<li>Allow 3 fallback relays per operator, which is safe now that we are choosing 200 fallback relays. Closes ticket <a href="https://bugs.torproject.org/20912" rel="nofollow">20912</a>.
  </li>
<li>Exclude relays affected by bug <a href="https://bugs.torproject.org/20499" rel="nofollow">20499</a> from the fallback list. Exclude relays from the fallback list if they are running versions known to be affected by bug <a href="https://bugs.torproject.org/20499" rel="nofollow">20499</a>, or if in our tests they deliver a stale consensus (i.e. one that expired more than 24 hours ago). Closes ticket <a href="https://bugs.torproject.org/20539" rel="nofollow">20539</a>.
  </li>
<li>Reduce the minimum fallback bandwidth to 1 MByte/s. Part of ticket <a href="https://bugs.torproject.org/18828" rel="nofollow">18828</a>.
  </li>
<li>Require fallback directories to have the same address and port for 7 days (now that we have enough relays with this stability). Relays whose OnionOO stability timer is reset on restart by bug <a href="https://bugs.torproject.org/18050" rel="nofollow">18050</a> should upgrade to Tor 0.2.8.7 or later, which has a fix for this issue. Closes ticket <a href="https://bugs.torproject.org/20880" rel="nofollow">20880</a>; maintains short-term fix in 0.2.8.2-alpha.
  </li>
<li>Require fallbacks to have flags for 90% of the time (weighted decaying average), rather than 95%. This allows at least 73% of clients to bootstrap in the first 5 seconds without contacting an authority. Part of ticket <a href="https://bugs.torproject.org/18828" rel="nofollow">18828</a>.
  </li>
<li>Annotate updateFallbackDirs.py with the bandwidth and consensus weight for each candidate fallback. Closes ticket <a href="https://bugs.torproject.org/20878" rel="nofollow">20878</a>.
  </li>
<li>Make it easier to change the output sort order of fallbacks. Closes ticket <a href="https://bugs.torproject.org/20822" rel="nofollow">20822</a>.
  </li>
<li>Display the relay fingerprint when downloading consensuses from fallbacks. Closes ticket <a href="https://bugs.torproject.org/20908" rel="nofollow">20908</a>.
  </li>
</ul>
</li>
<li>Minor features (geoip, also in 0.2.9.9):
<ul>
<li>Update geoip and geoip6 to the January 4 2017 Maxmind GeoLite2 Country database.
  </li>
</ul>
</li>
<li>Minor features (next-gen onion service directories):
<ul>
<li>Remove the "EnableOnionServicesV3" consensus parameter that we introduced in 0.3.0.1-alpha: relays are now always willing to act as v3 onion service directories. Resolves ticket <a href="https://bugs.torproject.org/19899" rel="nofollow">19899</a>.
  </li>
</ul>
</li>
<li>Minor features (linting):
<ul>
<li>Enhance the changes file linter to warn on Tor versions that are prefixed with "tor-". Closes ticket <a href="https://bugs.torproject.org/21096" rel="nofollow">21096</a>.
  </li>
</ul>
</li>
<li>Minor features (logging):
<ul>
<li>In several places, describe unset ed25519 keys as "&lt;unset&gt;", rather than the scary "AAAAAAAA...AAA". Closes ticket <a href="https://bugs.torproject.org/21037" rel="nofollow">21037</a>.
  </li>
</ul>
</li>
<li>Minor bugfix (control protocol):
<ul>
<li>The reply to a "GETINFO config/names" request via the control protocol now spells the type "Dependent" correctly. This is a breaking change in the control protocol. (The field seems to be ignored by the most common known controllers.) Fixes bug <a href="https://bugs.torproject.org/18146" rel="nofollow">18146</a>; bugfix on 0.1.1.4-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (bug resilience):
<ul>
<li>Fix an unreachable size_t overflow in base64_decode(). Fixes bug <a href="https://bugs.torproject.org/19222" rel="nofollow">19222</a>; bugfix on 0.2.0.9-alpha. Found by Guido Vranken; fixed by Hans Jerry Illikainen.
  </li>
</ul>
</li>
<li>Minor bugfixes (build):
<ul>
<li>Replace obsolete Autoconf macros with their modern equivalent and prevent similar issues in the future. Fixes bug <a href="https://bugs.torproject.org/20990" rel="nofollow">20990</a>; bugfix on 0.1.0.1-rc.
  </li>
</ul>
</li>
<li>Minor bugfixes (client, guards):
<ul>
<li>Fix bug where Tor would think that there are circuits waiting for better guards even though those circuits have been freed. Fixes bug <a href="https://bugs.torproject.org/21142" rel="nofollow">21142</a>; bugfix on 0.3.0.1-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (config):
<ul>
<li>Don't assert on startup when trying to get the options list and LearnCircuitBuildTimeout is set to 0: we are currently parsing the options so of course they aren't ready yet. Fixes bug <a href="https://bugs.torproject.org/21062" rel="nofollow">21062</a>; bugfix on 0.2.9.3-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (controller):
<ul>
<li>Make the GETINFO interface for inquiring about entry guards support the new guards backend. Fixes bug <a href="https://bugs.torproject.org/20823" rel="nofollow">20823</a>; bugfix on 0.3.0.1-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (dead code):
<ul>
<li>Remove a redundant check for PidFile changes at runtime in options_transition_allowed(): this check is already performed regardless of whether the sandbox is active. Fixes bug <a href="https://bugs.torproject.org/21123" rel="nofollow">21123</a>; bugfix on 0.2.5.4-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (documentation):
<ul>
<li>Update the tor manual page to document every option that can not be changed while tor is running. Fixes bug <a href="https://bugs.torproject.org/21122" rel="nofollow">21122</a>.
  </li>
</ul>
</li>
<li>Minor bugfixes (fallback directories):
<ul>
<li>Stop failing when a relay has no uptime data in updateFallbackDirs.py. Fixes bug <a href="https://bugs.torproject.org/20945" rel="nofollow">20945</a>; bugfix on 0.2.8.1-alpha.
  </li>
<li>Avoid checking fallback candidates' DirPorts if they are down in OnionOO. When a relay operator has multiple relays, this prioritizes relays that are up over relays that are down. Fixes bug <a href="https://bugs.torproject.org/20926" rel="nofollow">20926</a>; bugfix on 0.2.8.3-alpha.
  </li>
<li>Stop failing when OUTPUT_COMMENTS is True in updateFallbackDirs.py. Fixes bug <a href="https://bugs.torproject.org/20877" rel="nofollow">20877</a>; bugfix on 0.2.8.3-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (guards, bootstrapping):
<ul>
<li>When connecting to a directory guard during bootstrap, do not mark the guard as successful until we receive a good-looking directory response from it. Fixes bug <a href="https://bugs.torproject.org/20974" rel="nofollow">20974</a>; bugfix on 0.3.0.1-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (onion services):
<ul>
<li>Fix the config reload pruning of old vs new services so it actually works when both ephemeral and non-ephemeral services are configured. Fixes bug <a href="https://bugs.torproject.org/21054" rel="nofollow">21054</a>; bugfix on 0.3.0.1-alpha.
  </li>
<li>Allow the number of introduction points to be as low as 0, rather than as low as 3. Fixes bug <a href="https://bugs.torproject.org/21033" rel="nofollow">21033</a>; bugfix on 0.2.7.2-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (IPv6):
<ul>
<li>Make IPv6-using clients try harder to find an IPv6 directory server. Fixes bug <a href="https://bugs.torproject.org/20999" rel="nofollow">20999</a>; bugfix on 0.2.8.2-alpha.
  </li>
<li>When IPv6 addresses have not been downloaded yet (microdesc consensus documents don't list relay IPv6 addresses), use hard- coded addresses for authorities, fallbacks, and configured bridges. Now IPv6-only clients can use microdescriptors. Fixes bug <a href="https://bugs.torproject.org/20996" rel="nofollow">20996</a>; bugfix on b167e82 from 19608 in 0.2.8.5-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (memory leaks):
<ul>
<li>Fix a memory leak when configuring hidden services. Fixes bug <a href="https://bugs.torproject.org/20987" rel="nofollow">20987</a>; bugfix on 0.3.0.1-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (portability, also in 0.2.9.9):
<ul>
<li>Avoid crashing when Tor is built using headers that contain CLOCK_MONOTONIC_COARSE, but then tries to run on an older kernel without CLOCK_MONOTONIC_COARSE. Fixes bug <a href="https://bugs.torproject.org/21035" rel="nofollow">21035</a>; bugfix on 0.2.9.1-alpha.
  </li>
<li>Fix Libevent detection on platforms without Libevent 1 headers installed. Fixes bug <a href="https://bugs.torproject.org/21051" rel="nofollow">21051</a>; bugfix on 0.2.9.1-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (relay):
<ul>
<li>Honor DataDirectoryGroupReadable when tor is a relay. Previously, initializing the keys would reset the DataDirectory to 0700 instead of 0750 even if DataDirectoryGroupReadable was set to 1. Fixes bug <a href="https://bugs.torproject.org/19953" rel="nofollow">19953</a>; bugfix on 0.0.2pre16. Patch by "redfish".
  </li>
</ul>
</li>
<li>Minor bugfixes (testing):
<ul>
<li>Remove undefined behavior from the backtrace generator by removing its signal handler. Fixes bug <a href="https://bugs.torproject.org/21026" rel="nofollow">21026</a>; bugfix on 0.2.5.2-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (unit tests):
<ul>
<li>Allow the unit tests to pass even when DNS lookups of bogus addresses do not fail as expected. Fixes bug <a href="https://bugs.torproject.org/20862" rel="nofollow">20862</a> and 20863; bugfix on unit tests introduced in 0.2.8.1-alpha through 0.2.9.4-alpha.
  </li>
</ul>
</li>
<li>Code simplification and refactoring:
<ul>
<li>Refactor code to manipulate global_origin_circuit_list into separate functions. Closes ticket <a href="https://bugs.torproject.org/20921" rel="nofollow">20921</a>.
  </li>
</ul>
</li>
<li>Documentation (formatting):
<ul>
<li>Clean up formatting of tor.1 man page and HTML doc, where &lt;pre&gt; blocks were incorrectly appearing. Closes ticket <a href="https://bugs.torproject.org/20885" rel="nofollow">20885</a>.
  </li>
</ul>
</li>
<li>Documentation (man page):
<ul>
<li>Clarify many options in tor.1 and add some min/max values for HiddenService options. Closes ticket <a href="https://bugs.torproject.org/21058" rel="nofollow">21058</a>.
  </li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-232401"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-232401" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 24, 2017</p>
    </div>
    <a href="#comment-232401">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-232401" class="permalink" rel="bookmark">Yeah,</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yeah, AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA is kind of scary. It's like my computer is screaming.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-232545"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-232545" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 24, 2017</p>
    </div>
    <a href="#comment-232545">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-232545" class="permalink" rel="bookmark">ENCRYPT URL?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>ENCRYPT URL?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-232562"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-232562" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">January 25, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-232545" class="permalink" rel="bookmark">ENCRYPT URL?</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-232562">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-232562" class="permalink" rel="bookmark">What does this mean? Why do</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What does this mean? Why do you keep saying it?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-232602"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-232602" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 25, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-232602">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-232602" class="permalink" rel="bookmark">Automated bot. Hopefully you</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Automated bot. Hopefully you guys switch to a better blogging platform with a small captcha to ward off bots.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-232727"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-232727" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 25, 2017</p>
    </div>
    <a href="#comment-232727">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-232727" class="permalink" rel="bookmark">PLEASE IMPLEMENT THIS INTO</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>PLEASE IMPLEMENT THIS INTO TOR!</p>
<p><a href="http://ronray.net/scripts/URL-Encryption-II.html" rel="nofollow">http://ronray.net/scripts/URL-Encryption-II.html</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-232752"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-232752" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  yawning
  </article>
    <div class="comment-header">
      <p class="comment__submitted">yawning said:</p>
      <p class="date-time">January 26, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-232727" class="permalink" rel="bookmark">PLEASE IMPLEMENT THIS INTO</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-232752">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-232752" class="permalink" rel="bookmark">I assume you are the ENCRYPT</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I assume you are the ENCRYPT URL person.  Come back when you can explain how "URI percent-encoding" qualifies as encryption (hint: "It doesn't, at all") or how it will provide any security or obscurity what so ever (hint: "It provides neither").</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
