title: Tor Weekly News — March 25th, 2015
---
pub_date: 2015-03-25
---
author: harmony
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the twelfth issue in 2015 of Tor Weekly News, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what’s happening in the Tor community.</p>

<h1>Tor 0.2.4.26, 0.2.5.11, and 0.2.6.5-rc are out</h1>

<p>Nick Mathewson announced three new releases by the core Tor team. <a href="https://blog.torproject.org/blog/tor-02426-and-02511-are-released" rel="nofollow">Versions 0.2.4.26 and 0.2.5.11</a> are updates to the stable release series, featuring backports from later releases and an updated list of Tor directory authorities.</p>

<p><a href="https://blog.torproject.org/blog/tor-0265-rc-released" rel="nofollow">Tor 0.2.6.5-rc</a>, meanwhile, is the second release candidate in the upcoming Tor 0.2.6 series. It fixes a couple of possible crashes, and makes it easier to run Tor inside the Shadow network simulator. To find out more about all the new features that are expected in this release series, take a look at Nick’s <a href="https://blog.torproject.org/blog/coming-tor-026" rel="nofollow">guide</a> on the Tor blog.</p>

<p>Please see the release announcements for details of all changes, and download the source code from the <a href="https://dist.torproject.org/" rel="nofollow">distribution directory</a>.</p>

<h1>Tor Browser 4.0.5 is out</h1>

<p>Following the disclosure of two potentially serious security flaws in Firefox, the Tor Browser team <a href="https://blog.torproject.org/blog/tor-browser-405-released" rel="nofollow">announced</a> a pointfix release of the privacy-preserving browser. Tor Browser 4.0.5 is based on Firefox 31.5.3 ESR, fixing flaws in the <a href="https://www.mozilla.org/en-US/security/advisories/mfsa2015-28/" rel="nofollow">handling of SVG files</a> and <a href="https://www.mozilla.org/en-US/security/advisories/mfsa2015-29/" rel="nofollow">Javascript bounds checking</a> that could have allowed an adversary to run malicious code on a target machine.</p>

<p>This is an important security update, and all users of the stable Tor Browser should upgrade as soon as possible. Users of the alpha Tor Browser release channel will need to wait another week for an updated version; in the meantime, as Georg Koppen explained, they “are strongly recommended to use Tor Browser 4.0.5”. Download your copy of the new Tor Browser from the <a href="https://www.torproject.org/projects/torbrowser.html" rel="nofollow">project page</a>.</p>

<h1>Tails 1.3.1 is out</h1>

<p>The <a href="https://tails.boum.org/news/version_1.3.1/" rel="nofollow">Tails 1.3.1 emergency release</a> was put out on March 23, following the Firefox security announcement. As well as Tor Browser 4.0.5, this release includes updates to key software, fixing <a href="https://tails.boum.org/security/Numerous_security_holes_in_1.3/" rel="nofollow">numerous security issues</a>. All Tails users must upgrade as soon as possible; see the announcement for download instructions.</p>

<p>This release is also the first to be signed by the Tails team’s new OpenPGP signing key. For full details of the new key, see the team’s <a href="https://tails.boum.org/news/signing_key_transition/" rel="nofollow">announcement</a>.</p>

<h1>Who runs most of the Tor network?</h1>

<p>The Tor network is a diverse and mostly decentralized system, and it would not exist without the efforts of thousands of volunteer relay operators around the world. Some focus on the task of maintaining a single relay, while others set up “families” of nodes that handle a larger share of Tor traffic.</p>

<p>In an effort to identify the largest (publicly-declared) groupings of relays on the Tor network today, Nusenu <a href="https://lists.torproject.org/pipermail/tor-talk/2015-March/037305.html" rel="nofollow">posted</a> a list of entries found in the <a href="https://www.torproject.org/docs/faq.html.en#MultipleRelays" rel="nofollow">MyFamily field</a> of Tor relay configuration files, grouped by total “<a href="https://metrics.torproject.org/about.html#consensus-weight" rel="nofollow">consensus weight</a>”. This list also includes other relevant data such as the number of Autonomous Systems, /16 IP address blocks, and country codes in which these relays are located; as Nusenu says, “more is better” for these statistics, at least as far as diversity is concerned. If the concentration of relays in one location is too high, there is a greater risk that a single adversary will be able to see a large proportion of Tor traffic.</p>

<p>Nusenu also posted shorter lists of the largest relay families <a href="https://lists.torproject.org/pipermail/tor-relays/2015-March/006657.html" rel="nofollow">sorted by contact information</a>, and in the course of all this research was able to notify some relay operators of problems with their configuration. The future of the MyFamily setting is still being <a href="https://bugs.torproject.org/6676" rel="nofollow">discussed</a>; in the meantime, thanks to Nusenu for this impressive effort!</p>

<h1>Miscellaneous news</h1>

<p>Nathan Freitas <a href="https://lists.mayfirst.org/pipermail/guardian-dev/2015-March/004283.html" rel="nofollow">announced</a> Orbot version 15-alpha-5, bringing support for the meek and obfs4 pluggable transports, QR code bridge distribution, and other new features closer to a stable release.</p>

<p>George Kadianakis invited feedback on <a href="https://lists.torproject.org/pipermail/tor-dev/2015-March/008532.html" rel="nofollow">proposal 243</a>, which would require Tor relays to earn the “Stable” flag before they are allowed to act as onion service directories, making it harder for malicious relay operators to launch denial-of-service attacks on onion services.</p>

<p>Nick Mathewson <a href="https://lists.torproject.org/pipermail/tor-dev/2015-March/008502.html" rel="nofollow">asked for comments</a> on a list of possible future improvements to Tor’s controller protocol: “This is a brainstorming exercise, not a declaration of intent. The goal right now is to generate a lot of ideas and thoughts now, and to make decisions about what to build later.”</p>

<p>David Fifield <a href="https://lists.torproject.org/pipermail/tor-dev/2015-March/008473.html" rel="nofollow">wondered</a> why many of the graphs of Tor user numbers on the <a href="https://metrics.torproject.org" rel="nofollow">Metrics portal</a> appear to show weekly cycles.</p>

<p>Jens Kubieziel posted a <a href="https://lists.torproject.org/pipermail/tor-relays/2015-March/006670.html" rel="nofollow">list of ideas</a> for the further development of the Torservers organization, following recent discussions.</p>

<p>Mashael AlSabah and Ian Goldberg published “<a href="https://eprint.iacr.org/2015/235" rel="nofollow">Performance and Security Improvements for Tor: A Survey</a>”, a detailed introduction to the current state of research into performance and security on the Tor network. If you want to get up to speed on the most important technical questions facing the Tor development community, start here!</p>

<p>Aaron Johnson <a href="https://lists.torproject.org/pipermail/tor-talk/2015-March/037294.html" rel="nofollow">announced</a> that this year’s <a href="https://www.petsymposium.org/2015/hotpets.php" rel="nofollow">Workshop on Hot Topics in Privacy Enhancing Technologies (HotPETS)</a> is accepting two-page talk proposals, rather than full-length papers, in the hope that “this will make it even easier for more of the Tor community to participate, especially people who don’t write research papers for a living”. If you can offer “new ideas, spirited debates, or controversial perspectives on privacy (and lack thereof)”, see the Workshop’s website for submission guidelines.</p>

<p>This issue of Tor Weekly News has been assembled by Harmony, the Tails team, nicoo, and other contributors.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

