title: How Bandwidth Scanners Monitor The Tor Network
---
pub_date: 2019-04-11
---
author: juga
---
tags:

directory authority
bandwidth authority
collector
torflow
sbws
tor network
---
categories: network
---
summary: The Tor network is comprised of thousands of volunteer-run relays around the world, and millions of people rely on it for privacy and freedom online everyday. To monitor the Tor network's performance, detect attacks on it, and better distribute load across the network, we employ what we call Tor bandwidth scanners.
---
_html_body:

<p>The Tor network is comprised of thousands of volunteer-run relays around the world, and millions of people rely on it for privacy and freedom online everyday. To monitor the Tor network's performance, detect attacks on it, and better distribute load across the network, we employ what we call Tor bandwidth scanners. The bandwidth scanners are run by the directory authorities (dirauths).</p>
<p><img alt="tor-scanner" src="/static/images/blog/inline-images/tor-scanner_1.png" class="align-center" /></p>
<p><a href="https://metrics.torproject.org/glossary.html#relay">Tor relays</a> report their own bandwidth based on the traffic they have sent and received. But this reported bandwidth is not verified by other relays. Bandwidth scanners help verify relay bandwidths. They also provide some initial traffic to new relays, so those relays can report a useful amount of bandwidth.</p>
<p><a href="https://gitweb.torproject.org/torflow.git/tree/NetworkScanners/BwAuthority/README.spec.txt">Torflow</a> was the first Tor bandwidth scanner, started in 2011. Over time, it has become more difficult to install and to maintain, because the libraries it was built with are no longer maintained. In 2018, we started to develop "Simple Bandwidth Scanner" (<a href="https://gitweb.torproject.org/sbws.git/tree/README.md">sbws</a>) using more modern and maintained libraries. Right now, out of nine dirauths, six are bandwidth authorities, which means they run bandwidth scanners. (There is also one bridge authority. It doesn't do bandwidth scanning.)</p>
<p><a href="https://metrics.torproject.org/rs.html#search/flag:Authority"><img alt="Tor Bandwidth Authorities " src="/static/images/blog/inline-images/bandwidth-authorities.png" width="600" class="align-center" /></a></p>
<p>sbws chooses two relays, and builds a path between them. One relay is the target of the sbws measurement. The other relay is a random relay that's faster than the target relay. The scanner downloads data from a web server through this path between the relays. It measures the bandwidth as the amount of data downloaded and the time it took. Every hour, the scanner filters invalid measurements, aggregates them, and scales the valid ones. Finally, it writes a <a href="https://gitweb.torproject.org/torspec.git/tree/bandwidth-file-spec.txt">bandwidth file</a> with all the relays' bandwidth. The <a href="https://metrics.torproject.org/glossary.html#directory-authority">directory authorities</a> read this file and <a href="https://collector.torproject.org/recent/relay-descriptors/votes/">vote</a> on the relays' bandwidth.</p>
<p>Torflow divides the network into partitions depending on relay bandwidth. So some relays would end up stuck in a low-bandwidth partition. Unlike Torflow, sbws does not divide relays into partitions, so relays can't get stuck in a slow partition.</p>
<h2>Recent Updates</h2>
<p>To reach a consensus about a relay's bandwidth, as reported by the scanners, tor uses the median of at least three of their votes. Right now, there is only one directory authority running sbws, and five run Torflow. We plan to have three authorities running sbws by the end of April, so we'll start to see the effects of the changes to sbws soon. If all goes well, we'll eventually want all dirauths to switch from Torflow to sbws.</p>
<p>The latest version of sbws reports all relays that it has seen, including ones that it could not measure. This will help us to diagnose issues and anomalies in the relays, the network, and the software itself. It will also help to answer relay operators questions about their relay consensus weight and bandwidth.</p>
<p>In the next few months, we will start archiving the bandwidth files from sbws and Torflow using <a href="https://collector.torproject.org/recent/relay-descriptors/">CollecTor</a>. Once the directory authorities start running Tor version 0.4.0.4-alpha or later, <a href="https://trac.torproject.org/projects/tor/ticket/21378">CollecTor can ask them for their bandwidth files</a>. This will increase transparency while preserving anonymity, since the reported bandwidth values are aggregated from multiple measurements.</p>
<p>We also changed tor so it includes the <a href="https://gitweb.torproject.org/torspec.git/tree/dir-spec.txt?id=6e9c30c94b72d8f9cf22d5473214e4564403c638#n2154">bandwidth file hash</a> and <a href="https://gitweb.torproject.org/torspec.git/tree/dir-spec.txt?id=6e9c30c94b72d8f9cf22d5473214e4564403c638#n2134">bandwidth file headers</a> in the directory authority votes. In <a href="https://stem.torproject.org/tutorials/examples/check_digests.html">this diagram</a> you can see how the directory documents are related.</p>
<p>Before this change, it was possible to know the bandwidths that were reported by a <a href="https://gitweb.torproject.org/torspec.git/tree/dir-spec.txt?id=6e9c30c94b72d8f9cf22d5473214e4564403c638#n2384">scanner in a vote</a>, but we could not know which bandwidth file corresponded to which vote. The bandwidth file headers can also help to debug bandwidth file and vote issues.</p>
<p>We wrote a <a href="https://gitweb.torproject.org/torspec.git/tree/bandwidth-file-spec.txt">specification</a> for the bandwidth file format. This way, others can develop parsers to obtain metrics, or develop compatible bandwidth scanners.</p>
<h2>Future work</h2>
<p>There are a still several engineering and research <a href="https://trac.torproject.org/projects/tor/query?status=accepted&amp;status=assigned&amp;status=merge_ready&amp;status=needs_information&amp;status=needs_review&amp;status=needs_revision&amp;status=new&amp;status=reopened&amp;component=%5ECore+Tor%2Fsbws&amp;col=id&amp;col=summary&amp;col=status&amp;col=type&amp;col=priority&amp;col=milestone&amp;col=component&amp;order=priority">improvements</a> that can be done.</p>
<p>So far, sbws scales the raw bandwidth measurements in the same way as Torflow. Scaling is needed in order to balance the load in the network.</p>
<p>The measurement and scaling of bandwidth weights should achieve an <a href="https://trac.torproject.org/projects/tor/ticket/28582">equilibrium goal</a>. For instance, the user should experience consistent performance, regardless of the relays that their tor client has randomly chosen.</p>
<p>sbws is decentralized in the sense that there will be several instances of it running, but each of these instances is a single point of failure. Any shared servers or DNS infrastructure are also single points of failure.</p>
<p>Tor needs a minimum of three bandwidth authorities, and we have six bandwidth authorities running right now. We hope that sbws will be easy for directory authority operators to deploy, so we might have seven or eight authorities running a bandwidth scanner in the future.</p>
<p>sbws is still vulnerable to denial of services attacks and traffic manipulation, as explained in the 2018 <a href="https://blog.torproject.org/tors-open-research-topics-2018-edition">research post</a>.</p>
<h2>Get Involved</h2>
<p>We would be grateful to anyone who could help to improve the scanner. We encourage you to <a href="https://trac.torproject.org/projects/tor/newticket">open tickets</a>, base your implementations on sbws, develop a compatible external application programming interface, or extend the existing bandwidth file format. If implementations use similar code and data formats, it will be easier for Tor to use them, maintain them, and generate metrics from them.</p>
<p>If you want to dive deeper into some of the current bandwidth data, take a look at:</p>
<ul>
<li><a href="https://metrics.torproject.org/torperf.html">How long</a> it takes to download files over Tor.</li>
<li><a href="https://metrics.torproject.org/totalcw.html">Total consensus bandwidth</a> across directory authorities.</li>
<li><a href="https://metrics.torproject.org/bandwidth-flags.html">Total available bandwidth</a></li>
<li><a href="https://consensus-health.torproject.org/graphs.html">Consensus health graphs</a></li>
<li><a href="https://consensus-health.torproject.org/#overlap">Consensus health overlap</a></li>
</ul>
<h2>Acknowledgements</h2>
<p>sbws has been developed in conjunction with Tor network team and Tor community. It has been partly funded by <a href="https://prototypefund.de">Prototype Fund</a> under the name <a href="https://onbasca.readthedocs.io">OnBaSca</a>.</p>

---
_comments:

<a id="comment-280807"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-280807" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 11, 2019</p>
    </div>
    <a href="#comment-280807">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-280807" class="permalink" rel="bookmark">Wait, there are only 9…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Wait, there are only 9 dirauths and only 1 bridge auth out of about 6300 relays?  Isn't it bad to have so few?  Are these single points of failure related to why users sometimes can't connect to any onion sites?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-281101"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-281101" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  irl
  </article>
    <div class="comment-header">
      <p class="comment__submitted">irl said:</p>
      <p class="date-time">May 04, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-280807" class="permalink" rel="bookmark">Wait, there are only 9…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-281101">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-281101" class="permalink" rel="bookmark">Before version 3 of the Tor…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Before version 3 of the Tor directory protocol, each of the directory authorities was indeed a single point of failure. Now though they vote on the state of the network which means that adding directory authorities can increase trust as opposed to introducing more single points of failure. Even when all the directory authorities are offline, the network can still survive for a time using other relays as caches of the network status. We would like to increase the number of bridge authorities in the future and work is ongoing for allowing that, but the bridge authority does not need to be online for users to use bridges, only to learn about new bridges if they do not already know of any, or if the ones they are using are blocked.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-280835"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-280835" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Chris  Moore (not verified)</span> said:</p>
      <p class="date-time">April 13, 2019</p>
    </div>
    <a href="#comment-280835">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-280835" class="permalink" rel="bookmark">Excellent</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Excellent</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-280836"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-280836" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>KBD99603 (not verified)</span> said:</p>
      <p class="date-time">April 13, 2019</p>
    </div>
    <a href="#comment-280836">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-280836" class="permalink" rel="bookmark">Which, if any, of the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Which, if any, of the software listed here can be used by a normal Tor user <strong>and</strong> that automatically report the bandwidth results to the appropiate server? Because it sounds like only the bandwidth authorites are the ones reporting the results. </p>
<p>Thanks :)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-281102"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-281102" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  irl
  </article>
    <div class="comment-header">
      <p class="comment__submitted">irl said:</p>
      <p class="date-time">May 04, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-280836" class="permalink" rel="bookmark">Which, if any, of the…</a> by <span>KBD99603 (not verified)</span></p>
    <a href="#comment-281102">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-281102" class="permalink" rel="bookmark">Without the bandwidth…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Without the bandwidth authorities, we would have difficulty balancing load over the Tor network. Not all relays are equal and so we cannot uniformly distribute users over the relays. By using the bandwidth authorities to inform load balancing, every user of the network is indirectly benefitting from the bandwidth authorities though not running the code or interacting with it directly. We do not collect metrics from clients, including the tor client software or Tor Browser Bundle, as we do not yet have a means of performing this data collection in a way we consider to be safe. Tor relays aggregate the bandwidth used across all of its users to allow us to collect bandwidth usage statistics in a safer way, but we never see individual user bandwidth statistics.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
