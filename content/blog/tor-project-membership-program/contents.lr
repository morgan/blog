title: The Tor Project Membership Program
---
pub_date: 2020-08-31
---
author: alsmith
---
tags: fundraising
---
categories: fundraising
---
summary: Today we are officially launching the Tor Project Membership Program, a new way for nonprofit and private sector organizations to financially support our work.
---
_html_body:

<p>Today we are officially launching <a href="https://torproject.org/about/membership">the Tor Project Membership Program</a>, a new way for nonprofit and private sector organizations to financially support our work.</p>
<p>For a while, we have been thinking about how to continue to increase the diversity of funds in the Tor Project’s budget, and more importantly, how to increase <a href="http://www.businessdictionary.com/definition/unrestricted-funds.html">unrestricted funds</a>. The latest is a type of funding that allows us to be more agile with software development of tor and other tools.</p>
<p>We decided to create a program inspired by what Tor is based on, community. Our goal is to build a supportive relationship between our nonprofit and private sector organizations that use our technology or want to support our mission.</p>
<p><img alt="Avast believes that everyone, everywhere has the right to security and privacy in the digital world. We support Tor’s work to ensure that people can access and browse the web privately and freely. The Avast team is excited to be a founding partner of Tor’s membership program, and we look forward to working with Tor and other members to further our shared goal of empowering people with strong privacy protection and freedom online." src="/static/images/blog/inline-images/Quote %231.png" style="width:600px;height:465px;" class="align-center" /></p>
<p><strong>We are happy to make this announcement with five founding members:</strong></p>
<ul>
<li>Avast</li>
<li>DuckDuckGo</li>
<li>Insurgo</li>
<li>Mullvad VPN</li>
<li>Team Cymru</li>
</ul>
<p><strong>Why the membership program is important for the Tor Project:</strong></p>
<p>The traditional grants that nonprofits normally depend on, be that from governments or private foundations, have a long turn-around period (six to twelve months from submission of a proposal to the receipt of a contract and start of work). That means when a proposal is accepted and a grant contract is signed, we begin work on the project that we outlined sometimes more than a year prior.</p>
<p>Because we are a software development organization, relying only on grant funding, forces us into a development model that is slow and archaic. We can never execute solutions immediately in an agile way or experiment quickly with possible paths. We want to change that so we can respond to issues and start projects faster. And we can do that by increasing the number and amount of unrestricted contributions to the Tor Project. </p>
<p><img alt="To support the ongoing, critical work in defending online privacy, we have made a donation to The Tor Project. The Tor community has had a significant impact on the way Mullvad thinks about privacy, security, and censorship circumvention. The Tor Project is a nonprofit that believes everyone should be able to explore the internet with privacy. Support Tor with a donation, or consider becoming a sponsor." src="/static/images/blog/inline-images/Quote %233.png" style="width:600px;height:465px;" class="align-center" /></p>
<p><strong>More details on the Membership Program and how to get involved:</strong></p>
<p>For-profit and nonprofit organizations have supported Tor in the past. For example, DuckDuckGo and Mozilla have been long-time supporters. But there was something missing from these relationships. Everything else at Tor is based on community and relationship, so we decided to build a program that could bring some of that community support to our relationships with other organizations.</p>
<p>We know that many companies and organizations would appreciate direct contact with our team for support or consultation on privacy and security. The organizations that become members of our program will have access to our Onion Advisors group to help integrate Tor into their product or answer technical questions about privacy, censorship circumvention, and other areas of our expertise.</p>
<p><img alt="Insurgo provides its customers with the most open source hardware, firmware and software to provide more accessible security through Qubes OS compartmentalization. Tor is an integral part of its Qubes OS certified hardware solution." src="/static/images/blog/inline-images/Quote %232.png" style="width:600px;height:344px;" class="align-center" /></p>
<p>Members will also be invited to webinars and exclusive meetings with the Tor Project team to learn about what we are cooking at Tor.</p>
<p><strong>We created three tiers of Membership:</strong></p>
<p><img alt="Green Onion Membership, Vadalia Membership, Shallot Membership" src="/static/images/blog/inline-images/Membeship Program 5.png" class="align-center" /></p>
<p>Any membership level contribution means that your organization will have access to Onion Advisors and our special webinars. The only thing that differentiates the tiers is the public promotion of your membership. Each tier will come with varying opportunities to share your organization’s commitment to online privacy with our hundreds of thousands of followers and dedicated community.</p>
<p> If you are interested in becoming a member, please reach out to us at <a href="mailto:giving@torproject.org">giving@torproject.org</a>.</p>

---
_comments:

<a id="comment-289341"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289341" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 31, 2020</p>
    </div>
    <a href="#comment-289341">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289341" class="permalink" rel="bookmark">I&#039;m often impressed by Linux…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I'm often impressed by Linux Mint's lists of <a href="https://www.linuxmint.com/sponsors.php" rel="nofollow">sponsors</a> and <a href="https://www.linuxmint.com/donors.php" rel="nofollow">donors</a>. Clem posts them under <a href="https://blog.linuxmint.com/?p=3953" rel="nofollow">monthly news</a> announcement blogs. As far as I can see, their funds look unrestricted and mostly based on community relationships.  Skimming them once in a while, I found some cool sites I never saw before.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-289343"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289343" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  alsmith
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">Al Smith</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted"><a class="tor" title="View user profile." href="/user/194">Al Smith</a> said:</p>
      <p class="date-time">August 31, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-289341" class="permalink" rel="bookmark">I&#039;m often impressed by Linux…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-289343">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289343" class="permalink" rel="bookmark">Thanks! I&#039;ll take a look.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks! I'll take a look.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-289345"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289345" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 31, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-289343" class="permalink" rel="bookmark">Thanks! I&#039;ll take a look.</a> by <a class="tor" title="View user profile." href="/user/194">Al Smith</a></p>
    <a href="#comment-289345">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289345" class="permalink" rel="bookmark">It isn&#039;t just about who the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It isn't just about who the donors are. It's the way Clem publicizes them: monthly in footers of development posts, limiting each to small images or optional links, minimal javascript, care toward displayed names, optionally marking repeat donors, etc.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-289348"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289348" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  alsmith
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">Al Smith</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted"><a class="tor" title="View user profile." href="/user/194">Al Smith</a> said:</p>
      <p class="date-time">August 31, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-289345" class="permalink" rel="bookmark">It isn&#039;t just about who the…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-289348">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289348" class="permalink" rel="bookmark">Thanks for sharing, it&#039;s…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks for sharing, it's good to see how others are approaching this.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div><a id="comment-289352"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289352" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 31, 2020</p>
    </div>
    <a href="#comment-289352">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289352" class="permalink" rel="bookmark">But why Avast? Didn&#039;t they…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>But why Avast? Didn't they get caught selling user data?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-289383"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289383" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  alsmith
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">Al Smith</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted"><a class="tor" title="View user profile." href="/user/194">Al Smith</a> said:</p>
      <p class="date-time">September 04, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-289352" class="permalink" rel="bookmark">But why Avast? Didn&#039;t they…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-289383">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289383" class="permalink" rel="bookmark">Tor is open and transparent…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor is open and transparent about everything we do, from code to finances. We're pleased to accept support from organizations that understand the value of Tor's approach and support keeping Tor open. No sponsor or member will have influence over what we do with our product.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-289354"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289354" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Galaxy3 (not verified)</span> said:</p>
      <p class="date-time">September 01, 2020</p>
    </div>
    <a href="#comment-289354">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289354" class="permalink" rel="bookmark">A sense of community is…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>A sense of community is needed.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-289394"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289394" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>TheSkyis (not verified)</span> said:</p>
      <p class="date-time">September 06, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-289354" class="permalink" rel="bookmark">A sense of community is…</a> by <span>Galaxy3 (not verified)</span></p>
    <a href="#comment-289394">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289394" class="permalink" rel="bookmark">I agree wholeheartedly. In…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I agree wholeheartedly. In order for any changes or advancement in anything, it has to have the backing and support of it's supporters, community, and influencers.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-289364"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289364" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Macsin (not verified)</span> said:</p>
      <p class="date-time">September 02, 2020</p>
    </div>
    <a href="#comment-289364">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289364" class="permalink" rel="bookmark">It&#039;s great to see Companies…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It's great to see Companies supporting TOR.<br />
Thanks for all the works you guys doing.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-289365"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289365" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 02, 2020</p>
    </div>
    <a href="#comment-289365">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289365" class="permalink" rel="bookmark">Read the Docs is building a…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Read the Docs is building a business model they're calling Ethical Ads and describing as "newspaper advertising, on the internet."</p>
<p><a href="https://docs.readthedocs.io/en/stable/advertising/ethical-advertising.html" rel="nofollow">https://docs.readthedocs.io/en/stable/advertising/ethical-advertising.h…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-289380"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289380" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>publius (not verified)</span> said:</p>
      <p class="date-time">September 03, 2020</p>
    </div>
    <a href="#comment-289380">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289380" class="permalink" rel="bookmark">This seems like a great idea…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>This seems like a great idea. This helps the community know which organizations to support to better support the Tor Project, even if indirectly. After all, we're all in this together. Adding hyperlinks for each member would help people locate them (and ensure it's the correct company) more easily.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-289389"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289389" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 05, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-289380" class="permalink" rel="bookmark">This seems like a great idea…</a> by <span>publius (not verified)</span></p>
    <a href="#comment-289389">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289389" class="permalink" rel="bookmark">Your idea sounds related to…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Your idea sounds related to <a href="https://trac.torproject.org/projects/tor/wiki/org/projects/WeSupportTor" rel="nofollow">WeSupportTor</a>. Its third sentence is, "Tor users may wish to use, purchase from, support, and/or promote these services."</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-289381"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289381" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 04, 2020</p>
    </div>
    <a href="#comment-289381">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289381" class="permalink" rel="bookmark">Kinda not very impressed…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Kinda not very impressed with the message sent by having your "members" be organizations rather than, you know, humans.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-289382"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289382" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  alsmith
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">Al Smith</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted"><a class="tor" title="View user profile." href="/user/194">Al Smith</a> said:</p>
      <p class="date-time">September 04, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-289381" class="permalink" rel="bookmark">Kinda not very impressed…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-289382">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289382" class="permalink" rel="bookmark">We have other programs for…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>We have other programs for individual donors / humans. Defenders of Privacy (<a href="https://donate.torproject.org/monthly-giving" rel="nofollow">https://donate.torproject.org/monthly-giving</a>) and Champions of Privacy (<a href="https://donate.torproject.org/champions-of-privacy" rel="nofollow">https://donate.torproject.org/champions-of-privacy</a>). :)</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
