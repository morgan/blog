title: Alpha Bundles Available for Testing
---
pub_date: 2012-03-06
---
author: erinn
---
tags:

vidalia
tor browser bundle
alpha release
---
categories:

applications
releases
---
_html_body:

<p>There are new alpha Tor Browser Bundles and Vidalia Bundles available for testing!</p>

<p>These bundles include the latest Vidalia 0.3.1 alpha release and Tor 0.2.3.12-alpha.</p>

<p>Right now they are technology previews, so they aren't on the main download page yet, but please try them out and give us feedback in our <a href="https://trac.torproject.org" rel="nofollow">bug tracker</a>.</p>

<p><strong>Download links</strong></p>

<p><strong>Windows</strong></p>

<ul>
<li><a href="https://archive.torproject.org/tor-package-archive/technology-preview/tor-browser-2.3.12-alpha-1_en-US.exe" rel="nofollow">Tor Browser Bundle for Windows</a> (<a href="https://archive.torproject.org/tor-package-archive/technology-preview/tor-browser-2.3.12-alpha-1_en-US.exe.asc" rel="nofollow">sig</a>)</li>
</ul>

<p><strong>Mac OS X</strong></p>

<ul>
<li><a href="https://archive.torproject.org/tor-package-archive/technology-preview/TorBrowser-2.3.12-alpha-1-osx-i386-en-US.zip" rel="nofollow">Tor Browser Bundle for Mac OS X</a> (<a href="https://archive.torproject.org/tor-package-archive/technology-preview/TorBrowser-2.3.12-alpha-1-osx-i386-en-US.zip.asc" rel="nofollow">sig</a>)</li>
</ul>

<p><strong>Linux</strong></p>

<ul>
<li><a href="https://archive.torproject.org/tor-package-archive/technology-preview/tor-browser-gnu-linux-i686-2.3.12-alpha-1-en-US.tar.gz" rel="nofollow">Tor Browser Bundle for Linux (32-bit)</a> (<a href="https://archive.torproject.org/tor-package-archive/technology-preview/tor-browser-gnu-linux-i686-2.3.12-alpha-1-en-US.tar.gz.asc" rel="nofollow">sig</a>)</li>
<li><a href="https://archive.torproject.org/tor-package-archive/technology-preview/tor-browser-gnu-linux-x86_64-2.3.12-alpha-1-en-US.tar.gz" rel="nofollow">Tor Browser Bundle for Linux (64-bit)</a> (<a href="https://archive.torproject.org/tor-package-archive/technology-preview/tor-browser-gnu-linux-x86_64-2.3.12-alpha-1-en-US.tar.gz.asc" rel="nofollow">sig</a>)</li>
</ul>

<p>There are also normal Vidalia bundles available for Windows and 32-bit non-ppc OS X (10.5 and up) here:</p>

<p><strong>Windows</strong></p>

<ul>
<li><a href="https://archive.torproject.org/tor-package-archive/technology-preview/vidalia-bundle-0.2.3.12-alpha-0.3.1.exe" rel="nofollow">Vidalia Bundle for Windows</a> (<a href="https://archive.torproject.org/tor-package-archive/technology-preview/vidalia-bundle-0.2.3.12-alpha-0.3.1.exe.asc" rel="nofollow">sig</a>)</li>
</ul>

<p><strong>Mac OS X</strong></p>

<ul>
<li><a href="https://archive.torproject.org/tor-package-archive/technology-preview/vidalia-bundle-0.2.3.12-alpha-0.3.1-i386.dmg" rel="nofollow">Vidalia Bundle for Mac OS X</a> (<a href="https://archive.torproject.org/tor-package-archive/technology-preview/vidalia-bundle-0.2.3.12-alpha-0.3.1-i386.dmg.asc" rel="nofollow">sig</a>)</li>
</ul>

---
_comments:

<a id="comment-14512"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-14512" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 07, 2012</p>
    </div>
    <a href="#comment-14512">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-14512" class="permalink" rel="bookmark">HTTPS Everywhere 2.0</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>HTTPS Everywhere 2.0 development 4? Oh, come on, the point of an alpha package is not to make everything alpha, the most recent version is 2.0.1. Not to mention that development 4 is not even the last dev version there was.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-14513"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-14513" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 07, 2012</p>
    </div>
    <a href="#comment-14513">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-14513" class="permalink" rel="bookmark">Unlike the previous version</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Unlike the previous version (2.2.35-7.2), this alpha release seems to work well. I've just configured it and run the JonDonym test ...</p>
<p><a href="http://ip-check.info" rel="nofollow">http://ip-check.info</a></p>
<p>...with good results: 11x green and 5x ochre. Great!</p>
<p>- Not so great is that I couldn't use the last TBB because it was built with a newer GTK2 version than the TBB releases you have made over the past couple of months. I think such variations between releases are not ok. Maybe you can put your build environment on an ISO image that your co-conspirators can use when you take a break or go on holiday? The build process of an application that some people seriously depend upon really should be more standardized.</p>
<p>- The default settings in Firefox, Tor Button and NoScript are unnecessarily lax and some are outright dangerous. The selection of default Search Engines could also be improved. People have been complaining about this for some time and I'm wondering why nothing is changed release after release after release?</p>
<p>- When TBB starts up and there's a newer version available, Firefox opens an extra tab with information and a download link. This is good. But at the same time the locally configured home page is overwritten with the URL of the displayed page. And this is very, very bad! In fact, this is so bad that it reminds me of the worst excesses in the mid 1990s, when for a time anyone seemed to have access to one's browser configuration and bookmark files (Netscape, IE) ... completely unacceptable, especially in an application that is supposed to be secure and trustworthy. Since years my home page has been a 7 line piece of html that says: "home page". I like it that way because I don't want to go anywhere automatically when I start the web browser. And I certainly don't want some robot on the other side of the Atlantic to change my local browser configuration. It's stuff like this, that gets people speculating about what other remote control features there might be in TBB. Really, this has to go.</p>
<p>- This alpha version of Vidalia is still quite rough around the edges. I think, the yellow Post-It sticker saying ...</p>
<p>"Please wait a few seconds, a Tor enabled Firefox will start right away."</p>
<p>... shouldn't pop up somewhere in the middle of the screen. It makes the whole start up procedure look busy and confusing. It would be better if the notification appeared within the Vidala window itself.</p>
<p>- The network configuration in Vidalia has also been lacking for some time and it still is in this alpha version. The state of the tick box "My ISP blocks connections to the Tor network" is not stored correctly and keeps me guessing ... am I connected to the Tor network directly or through a bridge? There's no indicator for this. I think, when the box is visibly ticked and its state is stored, connections should be exclusively handled through bridges and if no bridge can be found then there should be no connection. This would give peace of mind to those who depend on Tor bridges.</p>
<p>- Another issue is the quality of the bridges. I have currently 10 bridge IPs stored in Vidalia. They've been there since months and I have no idea whether they're all working or if some of them are dead. A simple ping will not do to verify whether they are still acting as Tor bridges. Maybe a future version of Vidalia can monitor stored bridges and somehow indicate those that are no longer working? It would also be good if one could see which of the bridges is actually used for the connection.</p>
<p>- And lastly: since a couple of weeks I have been occasionally getting the error message ...</p>
<p>The proxy server is refusing connections<br />
Firefox is configured to use a proxy server that is refusing connections.</p>
<p>... when starting up TBB, and I have no idea why. It just happened again a few minutes ago with the new alpha version. But it also happened with version 2.2.35-7 from February. Does anyone know what could be the reason for this?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-14519"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-14519" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 08, 2012</p>
    </div>
    <a href="#comment-14519">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-14519" class="permalink" rel="bookmark">I cannot post in Trac, so am</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I cannot post in Trac, so am informing here: Bug # (forgot what one..) - 'Min/Max/Close' buttons are still blacked out if 'Menu Bar' is unticked, if ticked.. no issue. Also, by default JS (JaveScript) is enabled, Cookies are enabled, but not third-party (which, I feel is fine since 'Keep until: I close Firefox' is checked by default. 'Location Bar' is set to display, while typing 'History and Bookmarks': I have it set on 'Nothing' but this can also be done manually, which is fine. Others could by disabled etc by default but I'll just leave it at that.</p>
<p>Also, Scroogle SSL should be DuckDuckGo (HTTPS), as Scroogle doesn't work at times. There's also an hidden service for DuckDuckGo.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-14520"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-14520" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 08, 2012</p>
    </div>
    <a href="#comment-14520">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-14520" class="permalink" rel="bookmark">Also, that post I was using</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Also, that post I was using the Windows Bundle. More: In 'Firefox Updates' it is, and has been, by default set to 'Never check for updates': I set it at 'Check for updates, but let me choose whether to install them'.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-14533"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-14533" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 09, 2012</p>
    </div>
    <a href="#comment-14533">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-14533" class="permalink" rel="bookmark">Thank you for the alpha.
Did</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you for the alpha.</p>
<p>Did you feel it was unsafe to incorporate the extension RequestPolicy (as in the regular TBB)....<br />
or should we feel free to add it in ourselves?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-14535"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-14535" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 09, 2012</p>
    </div>
    <a href="#comment-14535">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-14535" class="permalink" rel="bookmark">This looks like an excellent</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>This looks like an excellent release with new, interesting features<br />
And it works for me whereas the previous 2.2.35-7.2 did not.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-14562"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-14562" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 12, 2012</p>
    </div>
    <a href="#comment-14562">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-14562" class="permalink" rel="bookmark">Tor Browser Bundle for Linux</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor Browser Bundle for Linux (32-bit) Alpha Bundle:</p>
<p>* Firefox:</p>
<p>- Crashes constantly, 99% of the time it asks with a pop-up window if<br />
  you'd like to restart the browser or other options. Choosing to reload<br />
  Firefox takes several seconds before it reloads, only to crash again<br />
  with normal use (no plugins - no addons other than what it ships with)<br />
- Crashes too often, making testing too frustrating to continue. I'm<br />
  certain crash logs would be of value if they could be submitted to<br />
  Tor devs rather than Mozilla devs.</p>
<p>* Vidalia:</p>
<p>- In tray, right click on BW/G, ML, NM options do not bring<br />
  Vidalia window(s) to the foreground, no effect.<br />
- In tray, right click on New Identity works (pop up notification)<br />
- In tray, right click on Control Panel works (brings up window)<br />
- In tray, right click on Settings works (brings up window)<br />
- In tray, right click on Help and About works (brings up window)<br />
-- When Control Panel window is on-screen and the tab<br />
   "Network Map" is selected, the left hand side of the window<br />
   frame shows nothing but a blank area, clicking on "refresh"<br />
   does not load any of the nodes with flags, instead it flashes<br />
   the only data visible, Connection/Status box. (same complaint<br />
   was registered on the Tor Blog during a previous testing release<br />
   by another tester)</p>
<p>* Suggestion:</p>
<p>- Offer some form of IRC applet or external link configured to<br />
  drop testers into your IRC channel to connect easily with<br />
  developers. Post this link on your blog, or only within testing<br />
  bundle posts.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-14568"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-14568" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 12, 2012</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-14562" class="permalink" rel="bookmark">Tor Browser Bundle for Linux</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-14568">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-14568" class="permalink" rel="bookmark">The 64-bit Alpha Bundle for</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The 64-bit Alpha Bundle for GNU/Linux works well, I don't know how much does it change with the 32-bit one, but maybe the problem is yours. The only bug I have noticed is that the message "Remember - Please wait a few seconds, a Tor enables Firefox will start right away..." displays in wrong place in the screen.<br />
There is no "Hide" button in Vidalia-alpha, and even if I can uncheck the "Show on startup", I liked to watch what was going on while Tor started and once connected hide Vidalia.<br />
And for first time for me, when I close the Tor enabled Firefox a window asks me what I want to do, something supposedly featured in previous versions, but that never worked for me.</p>
<p>Sorry for my bad english.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-14566"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-14566" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 12, 2012</p>
    </div>
    <a href="#comment-14566">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-14566" class="permalink" rel="bookmark">Can I update the preloaded</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Can I update the preloaded addons for FF through a Tor session without being MiTM'd?</p>
<p>I would like to update:</p>
<p>- Torbutton<br />
- NoScript<br />
- HTTPS-Everywhere</p>
<p>Through a TBB FF session.</p>
<p>Or, is the preferred (and secure) method to wait for new, stable releases with included updates for these addons?</p>
<p>Also:</p>
<p>** PLEASE ** PLEASE ** PLEASE ** PLEASE ** PLEASE **</p>
<p>In FF, inside about:config, under the Preference Name:</p>
<p>network.prefetch-next</p>
<p>the default value is true.</p>
<p>please change it to false.</p>
<p>in addition to being better for privacy/security it will save the tor network a lot of bandwidth as network.prefetch-next set to true preloads a lot of sites which the TBB user may never visit!</p>
<p>This is not related to the network.dns.disablePrefetch entry.</p>
<p>Thanx!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-14571"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-14571" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 12, 2012</p>
    </div>
    <a href="#comment-14571">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-14571" class="permalink" rel="bookmark">Why are you suppressing</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Why are you suppressing comments, Erinn? Last Wednesday I posted here a perfectly reasonable comment ... no swearing words, no insults, no personal attacks of any kind, just some observations and some simple questions and suggestions ... and yet, you didn't let it through. What's the matter?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-14578"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-14578" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 13, 2012</p>
    </div>
    <a href="#comment-14578">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-14578" class="permalink" rel="bookmark">Empty field with country</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Empty field with country flags (microdescriptors) in "network map" window and shows "0 relays online" but works. Annoying bug.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-14588"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-14588" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 14, 2012</p>
    </div>
    <a href="#comment-14588">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-14588" class="permalink" rel="bookmark">Working on my Mac 10.7.3 Good</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Working on my Mac 10.7.3 Good</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-14607"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-14607" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 16, 2012</p>
    </div>
    <a href="#comment-14607">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-14607" class="permalink" rel="bookmark">wheres the new update since</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>wheres the new update since firefox updated to ff 11</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-14709"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-14709" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 20, 2012</p>
    </div>
    <a href="#comment-14709">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-14709" class="permalink" rel="bookmark">The Mac OSX one dose not</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The Mac OSX one dose not work on a 10.5.8. Actually Tor quit working on my computer all together I would like some help. It just opens and closes right away. I use  to run TBB for OSX what happen.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-14781"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-14781" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 26, 2012</p>
    </div>
    <a href="#comment-14781">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-14781" class="permalink" rel="bookmark">running on Ubuntu 10.04 LTS</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>running on Ubuntu 10.04 LTS (32bit) , I got an error. Please help me.....</p>
<p>$ ./start-tor-browser </p>
<p>Launching Tor Browser Bundle for Linux in /home/dennis/downloads/browser/tor/tor-browser_en-US<br />
Qt: Session management error: None of the authentication protocols specified are supported</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-14992"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-14992" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 15, 2012</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-14781" class="permalink" rel="bookmark">running on Ubuntu 10.04 LTS</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-14992">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-14992" class="permalink" rel="bookmark">Same problem what do you do?!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Same problem what do you do?!</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-16236"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-16236" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 18, 2012</p>
    </div>
    <a href="#comment-16236">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-16236" class="permalink" rel="bookmark">When TBBs runs on</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>When TBBs runs on Ubuntu12.04, it crashed few seconds later</p>
</div>
  </div>
</article>
<!-- Comment END -->
