title: Hackers Next Door (Brooklyn)
---
author: steph
---
start_date: 2019-12-14
---
end_date: 2019-12-15
---
body:

Hackers Next Door is an open-to-the-public information security and social technology conference featuring presentations by the best of the tri-state area’s cybersecurity trainers, privacy rights advocacy groups, activist networks, and their constituencies.

The two-day conference is a unique opportunity for anyone interested in protecting themselves online to participate in a skill-sharing and educational convergence where they can meet one another, compare notes with peers and colleagues, and learn practical techniques from the region’s top cybersecurity instructors, legal experts, and experienced activists.

For community organizers and movement builders, Hackers Next Door offers a weekend of hacking demonstrations, “State of the Movement” briefings, legal primers, know-your-rights trainings, technical workshops, and political strategy brainstorming opportunities in a kinetic environment.

By enhancing cybersecurity training for everyone and increasing public awareness of digital civil liberties issues, Hackers Next Door gives invited speakers a chance to showcase and workshop their educational methods and materials to and with an audience of like-minded and impassioned community organizers so that we can all learn from one another’s experience and perspective.

View the list of [confirmed speakers](https://hnd.techlearningcollective.com/speakers/), [conference sessions](https://hnd.techlearningcollective.com/sessions/), and [buy tickets](https://hnd.techlearningcollective.com/tickets/) today.

Taking Back the Internet with Tor
=================================

Presented by [Isabela Bagueros](https://hnd.techlearningcollective.com/speakers/#isabela-bagueros)

Tracking, surveillance, and censorship are widespread online, but Tor tools, including Tor Browser and onion services, empower you to take back the internet.

The original vision of the internet was a free and open space where you could share resources and communicate freely, a decentralized space where your identity didn’t matter. When powerful entities recognized that huge profits could be made online, the technology that made up the internet was co-opted to form a centralized, traceable, ‘self-centered,’ system. The decentralized vision of the internet faded from the consciousness of the general public. In this session, we’ll look at what happened to the internet to make Tor necessary and how Tor can help you take back your privacy and freedom online.

