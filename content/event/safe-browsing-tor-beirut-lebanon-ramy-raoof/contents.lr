title: Safe Browsing with Tor, Beirut, Lebanon (Ramy Raoof)
---
author: steph
---
start_date: 2017-11-27
---
body:

As purchasing new spying tools by Lebanon provides yet another proof of the increased level of surveillance, and as there is no legal way to protect users, SMEX cordially invites you to a talk about introductory concepts and protection measures related to our use of the internet. The event will take place on Monday, November 27, at 6 pm in SMEX office, 4th floor, Kmeir Building, Badaro. 

During this event we will tackle various angles; get to know how the internet works, what Tor is, what Virtual Private Networks (VPNs) are, and how we can browse the internet safely and protect our online privacy. You can ask questions and get to know how to best engage with technology. We will mix between opening discussions and an open mic for all participants in order to make them interact with the topic and get to know it better. Among the questions that we will try to answer together: 

What path does our data take on the internet? How can journalists publish and receive documents safely and anonymously?How can people seek to know about intimate and private issues privately? 

Our guest, Ramy Raoof, a researcher in information security and a privacy consultant, currently on a volunteering visit to Beirut, will moderate the discussion and answer your questions. 

This event will be in Arabic with the possibility of providing on-spot English interpretation. 

**About SMEX**  

SMEX is a registered non-profit association based in Beirut that works to advance self-regulating information societies. Our mission is to defend digital rights, promote open culture and local content, and encourage critical, engagement with digital technologies, media, and networks through research, knowledge-sharing, and advocacy. 

**About Ramy Raoof**  

Ramy is a technologist, privacy, and security researcher. He interacts with a wide spectrum ranging from NGOs, journalists, lawyers, politicians, and artists on the intersection of tech and social causes, mainly on privacy and security, by devoting his skills as a techie and passion for free/open culture. Ramy develops privacy protocols, and in his research, he focuses on surveillance patterns and holistic privacy. In the course of his work, he has provided and developed digital security plans and strategies for NGOs and members of the media, emergency response in cases of physical threats, support on publishing sensitive materials, secure systems for managing sensitive information, and operational plans for human rights emergency response teams, in Egypt and the MENA region. Most recently, Ramy has been volunteering with different NGOs and civil liberty groups in Central & South America, to enhance their privacy and security through means of behavioral change based on understanding surveillance and threat models in their own contexts and environments. 

In October 2017, Ramy received the 2017 international award Heroes of Human Rights and Communications Surveillance, "for exhaustive efforts to reveal invasive and harmful surveillance tactics that are being used to harm users at risk.", by Access Now. In May 2016 he received the international Bobs Award - Best of Online Activism in recognition for his work in digital security and privacy. In January 2016, he was named in the World Top 100 Info Security Influencers list by CISO Platform in India. In 2012, he was ranked number 10 by the Newsweek Magazine in the Revolutionaries list of the Digital Power Index, after being ranked number 14 by Forbes Middle East in Top 100 Arab Presence on Twitter a year earlier. 

Among different hats, Ramy is Senior Research Technologist at the Egyptian Initiative for Personal Rights (EIPR), Research Fellow with Citizen Lab, and he was a volunteer visitor with Fundación Acceso assisting collectives and networks in Central America around infosec and activism. He is also Internet Freedom Festival Fellow on security and privacy best practices. Most recently, Ramy joined the Board of Directors in The Tor Project. 

Website:
[Facebook Event](https://www.facebook.com/events/1919059708354226/)
---
tags:

tor browser
talks
security workshop
